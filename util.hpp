#ifndef UTIL_HPP_INCLUDED
#define UTIL_HPP_INCLUDED

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <cassert>
#include <vector>

#include <tinyxml2.h>

static void str_lower(std::string& s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
}

static bool get_attribute(const tinyxml2::XMLElement *e, const char* a, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Attribute(a);
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_name(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Name();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_value(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Value();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_text(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->GetText();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}

static void str_tabify(std::string& s, const char c = ' ', const size_t tabsize = 4, const size_t min_width = 0)
{
    // "ab"   > "ab__"     2 -> 2
    // "a"    > "a___"
    // "abc"  > "abc_"     3 -> 1
    // "abcd" > "abcd____" 0 -> 4

    {
        size_t len = s.length();
        while (len < min_width) { s += c; ++len; }
    }
    const size_t len = s.length();
    size_t i = (len % tabsize);
    i = tabsize - i;
    while (i--) { s += c; }

    #if 0
    const size_t len = s.length();
    size_t i = (len % tabsize);
    i = tabsize - i;
    while (i--) { s += c; }
    #endif // 0
}
static void str_rightalign(std::string& s, const char c = ' ', const size_t width = 4)
{
    // "a"  > "___a"
    // "ab" > "__ab"
    if (s.length() >= width) { return; }
    const size_t dx = width - s.length();
    std::string f; f.assign(dx, c);
    s = f + s;
}

static size_t str_tabify_col(std::vector<std::string>& col, const char c = ' ', const size_t tabsize = 4, const size_t min_width = 0)
{
    size_t maxw = 0;
    for (const std::string& s : col)
    {
        const size_t curw = s.length();
        if (curw > maxw) { maxw = curw; }
    }
    if (maxw < min_width) { maxw = min_width; }

    for (std::string& s : col)
    {
        str_tabify(s, c, tabsize, maxw);
    }
    return maxw;
}
static size_t str_rightalign_col(std::vector<std::string>& col, const char c = ' ', const size_t tabsize = 4, const size_t width = 4)
{
    size_t maxw = 0;
    for (const std::string& s : col)
    {
        const size_t curw = s.length();
        if (curw > maxw) { maxw = curw; }
    }
    //if (maxw < min_width) { maxw = min_width; }
    // round maxw up to multiples of tabsize
    if (tabsize > 0)
    {
        if ((maxw%tabsize) != 0) { maxw = (maxw/tabsize + 1)*tabsize; }
    }

    for (std::string& s : col)
    {
        str_rightalign(s, c, maxw);
    }
    return maxw;
}

static bool str_compare(const char* s0, const char* s1) /// true on match
{
    if ((s0 == nullptr) || (s1 == nullptr))
    {
        return (s0 == s1);
    }
    //size_t i =
    while (true)
    {
        if (*s0 != *s1) { return false; }
        if (*s0 == '\0') { return true; }
        ++s0; ++s1;
    }
}
static bool str_compare(const std::string& s0, const std::string& s1) /// true on match
{
    if (s0.length() != s1.length()) { return false; }
    return (s0.compare(s1) == 0);
}

static std::size_t unused_size_t;
static bool str_hex2bin(const std::string& mask, uint64_t& out, std::size_t& bytes_used = unused_size_t)
{
    out = 0;
    std::size_t tbu = 0;
    if (mask.length() < 3) { return false; }
    if ((mask[0] == '0') && (mask[1] == 'x'))
    {
        if ((mask.length()-2) > (64/4))
        {
            std::cout << "* Error: bitmask longer than 64bits not supported.\n";
            return false;
        }
        std::size_t i = 2;
        uint64_t x = 0;
        while (i < mask.length())
        {
            char c = mask[i];
            if      ((c >= '0') && (c <= '9')) { c -= '0'; }
            else if ((c >= 'A') && (c <= 'F')) { c = ((c - 'A') + 10); }
            else if ((c >= 'a') && (c <= 'f')) { c = ((c - 'a') + 10); }
            else
            {
                std::cout << "* Error: unexpected symbol in bitmask.\n";
                return false;
            }
            x = (x<<4) | c;
            if ((c != 0) || (tbu != 0)) { ++tbu; }
            ++i;
        }
        out = x;
        bytes_used = tbu/2;
    }
    return true;
}
static bool str_dec2bin(const std::string& val, uint64_t& out)
{
    if (val.length() < 1) { return false; }
    size_t i = 0;
    out = 0;
    while (i < val.length())
    {
        char c = val[i];
        if ((c >= '0') && (c <= '9')) { c -= '0'; out = out*10 + c; }
        else { return false; }
        ++i;
    }
    return true;
}
static bool str_lazy2bin(const std::string& val, uint64_t& out, std::size_t& bytes_used = unused_size_t)
{
    uint64_t res = 0;
    if      (str_hex2bin(val, res, bytes_used)) { out = res; return true; }
    else if (str_dec2bin(val, res))             { out = res; return true; }
    return false;
}

/*
        {
            bool err = false;
            std::string errv;
            if (name.empty()) { err = true; errv += " name"; }
            if (size.empty()) { err = true; errv += " size"; }
            if (err) { cout << "* Error: register-group without " << errv << "? (line:" << e->GetLineNum() << ")\n"; }
        }
*/

template <typename T>
void str_bin2hex(T x, std::string& s)
{
    //size_t i = 0;
    if (x == 0) { s = "0x00";}
    std::string t;
    while (x)
    {
        const uint8_t b = x&0xFF;
        x = (x>>8);
        const uint8_t b1 = b>>4;
        const uint8_t b0 = b&0x0F;
        static constexpr char lut_hex[16] =
        { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        t += lut_hex[b0];
        t += lut_hex[b1];
    }
    s.clear();
    size_t i = 0;
    const size_t n = t.length();
    while (i < n)
    {
        ++i;
        s += t[n-i];
    }
}

static bool str_rem_newlines(std::string& s) /// true if modified
{
    std::size_t pos0 = 0;
    std::size_t pos1 = 0; // = str.find("\n");
    std::string res;
    bool rrr = false;
    while (true)
    {
        pos1 = s.find('\n',pos0);
        std::string str1 = s.substr(pos0, (pos1-pos0));
        if (!str1.empty())
        {
            if (!res.empty()) { res += '_'; rrr = true; }
            res += str1;
        }
        if (pos1 == std::string::npos) { break; }
        pos0 = pos1+1;
    }
    //const bool rrr = (s.compare(res) == 0);
    s = res;
    return rrr;
}
static bool str_rem_newlines2(std::string& s) /// true if modified
{
    // this version particularly deals with the case where
    // some SVDs seem to have:
    //   <description>Text split\n
    //     into multiple lines\n
    //     and also indented</description>
    // it seems the indented line may have \t tab chars too
    bool rrr = false;
    {
        std::size_t i = 0;
        std::string res;
        const std::size_t n = s.length();
        res.reserve(n);
        char c1 = '\0';
        char c0 = '\0';
        while (i < n)
        {
            c1 = c0;
            c0 = s[i];
            if (c0 == '\t') { c0 = ' '; rrr = true; } // replace \t with space
            if      (c0 == ' ')
            {
                if (c1 == ' ') { ++i; rrr = true; continue; }
            }
            res += c0;
            ++i;
        }
        if (rrr) { s = res; }
    }
    std::size_t pos0 = 0;
    std::size_t pos1 = 0; // = str.find("\n");
    std::string res;
    while (true)
    {
        pos1 = s.find('\n',pos0);
        std::string str1 = s.substr(pos0, (pos1-pos0));
        bool addspace = false;
        if (!str1.empty())
        {
            if (!res.empty())
            {
                rrr = true;
                if (res.at(res.length()-1) == ' ') { addspace = false; }
                else { addspace = true; }
            }
            std::size_t i = 0;
            while (i < str1.length())
            {
                if (str1[i] != ' ') { break; }
                ++i;
            }
            if (i > 0) { str1 = str1.substr(i, str1.length()-i); }
            if (addspace) { res += ' '; }
            //std::cout << "(" << res << ")   " << "(" << str1 << ")\n";
            res += str1;
        }
        if (pos1 == std::string::npos) { break; }
        pos0 = pos1+1;
    }
    //const bool rrr = (s.compare(res) == 0);
    s = res;
    return rrr;
}
static bool str_rem_surrounding_chars(std::string& s, const std::string what) /// true if modified
{
    if (s.length() < what.length()) { return false; }
    bool rrr = false;
    std::string res = s;
    {
        std::size_t pos0 = res.length();
        std::size_t pos1 = pos0 - what.length();
        if (res.substr(pos1,what.length()) == what)
        {
            res = res.substr(0, pos1);
            rrr = true;
        }
    }
    {
        if (res.substr(0,what.length()) == what)
        {
            res = res.substr(what.length(), res.length()-what.length());
            rrr = true;
        }
    }
    s = res;
    return rrr;
};

static bool str_replace(std::string& s, const std::string what, const std::string with_what = "")
{
    const size_t p0 = s.find(what);
    if (p0 == std::string::npos) { return false; }
    s.replace(p0, what.length(), with_what);
    //s.replace(s.find(what), what.length(), with_what);
    return true;
}
static bool str_replace_all(std::string& s, const std::string what, const std::string with_what = "")
{
    const size_t wlen = what.length();
    size_t p0 = 0;
    bool res = false;
    while (true)
    {
        const size_t p1 = s.find(what, p0);
        if (p1 == std::string::npos) { break; }
        res = true;
        s.replace(p1, wlen, with_what);
        p0 = p1 + wlen;
    }
    return res;
}

// #############################################################################
template<typename T = uint64_t>
struct fumber_t /// Fancy nUMBER ... because i didn't have a better name for this
{
    enum TYPE
    {
        T_BOOL,     //!< false/0/true/1
        T_BOOLC,    //!< false/true
        T_BOOLD,    //!< 0/1
        T_DECIMAL,  //!< 123
        T_HEX,      //!< 0x0F2C
        T_BIN,      //!< #10010000
        T_BIN2,     //!< 0b10010000
        T_ERROR     //!< Som Ting Wong
    };
    TYPE type;
    size_t num_chars; // the 0x, #, and 0b are not counted
    T val;
    fumber_t() : type(T_ERROR), num_chars(1), val(0)
    {}

    bool good() const
    {
        // max num_chars worst case should be the number of bits
        return ((type >= T_BOOL) && (type < T_ERROR) && (num_chars <= (2*8*sizeof(T))));
    }

    bool set(const std::string& s)
    {
        type = T_ERROR;
        if (s.empty()) { return false; }
        val = 0;
        const size_t len = s.length();
        size_t i = 0;
        bool neg = false;
        if constexpr (std::is_same<T, bool>::value)
        {
            if      (len == 1)
            {
                if      (s[0] == '0')   { val = 0; i = len; }
                else if (s[0] == '1')   { val = 1; i = len; }
            }
            else if (len == 4)
            {
                if (s == "true") { val = true; i = len; }
            }
            else if (len == 5)
            {
                if (s == "false") { val = false; i = len; }
            }
            if ((i == len) && (len > 0))
            {
                    type = T_BOOL;
                    if (i >= 4) { type = T_BOOLC; }
                    num_chars = i;
                    return true;
            }
            else { return false; }
        }
        {
            // try decimal, if fail due to weird chars - try the other ways
            if (len >= 2)
            {
                const char c = s[0];
                if (c == '-') { neg = true; ++i; }
                else if (c == '+') { ++i; }
            }
            while (i < len)
            {
                char c = s[i];
                if ((c >= '0') && (c <= '9')) { val = 10*val+(c-'0'); }
                else { break; }
                ++i;
            }
            if (i == len)
            {
                num_chars = i;
                type = T_DECIMAL;
                if (neg) { val = -val; }
                return true;
            }
        }
        if ((s[0] == '#') && (len > 1)) // cmsis-svd kind of #binary
        {
            i = 1;
            while (i < len)
            {
                char c = s[i];
                if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                else { return false; }
                ++i;
            }
            num_chars = len-1;
            type = T_BIN;
            return true;
        }
        else if ((s[0] == '0') && (len > 2))
        {
            i = 2;
            if (s[1] == 'b') // binary2
            {
                while (i < len)
                {
                    char c = s[i];
                    if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                    else { return false; }
                    ++i;
                }
                num_chars = len-2;
                type = T_BIN2;
                return true;
            }
            else if (s[1] == 'x') // hex
            {
                while (i < len)
                {
                    char c = s[i];
                    if      ((c >= '0') && (c <= '9')) { c -= '0'; }
                    else if ((c >= 'A') && (c <= 'F')) { c = (10+c-'A'); }
                    else if ((c >= 'a') && (c <= 'f')) { c = (10+c-'a'); }
                    else { return false; }
                    val = (val<<4) | c;
                    ++i;
                }
                num_chars = len-2;
                type = T_HEX;
                return true;
            }
        }
        return false;
    }

    void operator= (const std::string& s) { set(s); }
    void operator= (const char* s) { set(s); }

    template<typename T2> void operator= (const T2& x) { val = x; }
    operator T() { return val; }

    std::string str_safe() const noexcept
    {
        if (!good())
        {
            if constexpr (std::is_same<T, bool>::value) { return "<bad_fumber_bool>"; }
            return "<bad_fumber>";
        }
        return str();
    }
    std::string str() const
    {
        static constexpr char lut_bin_nib[] =
        {
            "0000" "1000" "0100" "1100" "0010" "1010" "0110" "1110"
            "0001" "1001" "0101" "1101" "0011" "1011" "0111" "1111"
        };
        std::string s = "#";
        switch (type)
        {
            case T_BOOLC:
                s = (val == 0 ? "false" : "true");
                return s;
            case T_BOOLD:
                [[fallthrough]];
            case T_BOOL:
                [[fallthrough]];
            case T_DECIMAL:
            {
                s = std::to_string(val);
                if (s.length() < num_chars) { std::string t(num_chars - s.length(), ' '); s = t + s; }
                return s;
            } break;
            case T_HEX:
            {
                // try to fit it into "num_chars"
                // if it's shorter - add zeros in front
                //size_t i = 0;
                //if (val == 0) { s = "0";}
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4);
                    const uint8_t b0 = (b&0x0F);
                    static constexpr char lut_hex[16] =
                    {
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
                    };
                    t += lut_hex[b0];
                    t += lut_hex[b1];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                s = "0x";
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_BIN2:
                s = "0b";
                [[fallthrough]];
            case T_BIN:
            {
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4)<<2;
                    const uint8_t b0 = (b&0x0F)<<2;
                    t += lut_bin_nib[b0];
                    t += lut_bin_nib[b0+1];
                    t += lut_bin_nib[b0+2];
                    t += lut_bin_nib[b0+3];
                    t += lut_bin_nib[b1];
                    t += lut_bin_nib[b1+1];
                    t += lut_bin_nib[b1+2];
                    t += lut_bin_nib[b1+3];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_ERROR: break;
            // default: don't put a default!
        };
        s = "<error>";
        std::cerr << "* Error: bad fumber conversion!\n";
        throw std::logic_error("this fumber was bad, cannot convert it to string.");
        return s;
    }
    void recalc_style()
    {
        // recalc how many chars are needed to fit the current val
        switch (type)
        {
            case T_BIN:
                [[fallthrough]];
            case T_BIN2:
            {
                num_chars = 0;
                T x = val;
                while (x != 0) { x = (x >> 1); ++num_chars; }
                if (num_chars == 0) { num_chars = 1; }
            } break;

            case T_HEX:
            {
                num_chars = 0;
                T x = val;
                while (x != 0) { x = (x >> 8); num_chars += 2; }
                if (num_chars == 0) { num_chars = 2; }
            } break;

            case T_DECIMAL:
            {
                num_chars = 0;
                T x = val;
                while (x != 0)
                {
                    x /= 10;
                    ++num_chars;
                }
                if (num_chars == 0) { num_chars = 1; }
            } break;
            case T_BOOLC:
            {
                if (val == 0) { num_chars = 5; }
                else { num_chars = 4; }
            } break;
            case T_BOOLD:
                [[fallthrough]];
            case T_BOOL:
            {
                num_chars = 1;
            } break;
            case T_ERROR: break;
        }
    }
    bool set_if_larger(const fumber_t<T>& a)
    {
        if (a.val > val) { val = a.val; return true; }
        return false;
    }
    void copy_style(const fumber_t<T>& a)
    {
        if (a.type < T_ERROR) { type = a.type; num_chars = a.num_chars; }
    }
    void grow_style(const fumber_t<T>& target)
    {
        set_if_larger(target);
        copy_style(target);
    }
};

struct skip_first_delimiter
{
    skip_first_delimiter(const std::string _what) : once(true), what(_what) {}
    std::string operator()()
    {
        const bool x = once;
        once = false;
        if (x == false) { return what; }
        return std::string("");
    }
    bool once;
    std::string what;
};
struct skip_last_delimiter
{
    skip_last_delimiter(const std::string _what, const size_t _count) : n(_count), what(_what)
    {
        if (n > 0) { --n; }
    }
    std::string operator()()
    {
        if (n == 0) { return std::string(""); }
        --n;
        return what;
    }
    size_t n;
    std::string what;
};

template <typename X>
struct scoped_vector_element
{
    // creates a new element at the end of the vector
    // removes it when it goes out of scope
    using _vecT = std::vector<X>;
    scoped_vector_element(_vecT &_vec)
    : okay(false), ptr(nullptr), vec(_vec)
    {
        ptr = &vec.emplace_back();
        okay = true;
    }

    ~scoped_vector_element()
    {
        if ((okay) && (!vec.empty()))
        {
            vec.erase(vec.end()-1);
        }
    }
    X& get()
    {
        if (okay)
        {
            return *ptr;
        }
        throw std::logic_error("scoped_vector_element.get() called multiple times");
    }
    private:
    bool okay;
    X *ptr;
    _vecT& vec;
};

namespace svd_util
{
constexpr size_t cg_indent_level_max = 64; // should be good enough for anyone
struct cg_indent_base
{
    cg_indent_base()
    : lvl(0), s("")
    {}
    cg_indent_base(size_t _level)
    : lvl(_level)
    { calc(); }

    void set_level(size_t i)
    {
        lvl = i;
        calc();
    }
    void calc() {
        s.erase();
        if (lvl > cg_indent_level_max) { lvl = cg_indent_level_max; }
        size_t i = lvl;
        while (i--) { s += "    "; }
    }

    inline const std::string& operator()() const noexcept { return s; }

    size_t lvl;
    std::string s;
};
} // namespace svd_util
struct cg_indent : protected svd_util::cg_indent_base //!< code-gen helper object for indentation
{
    cg_indent()// : lvl(0), s("")
    {}
    using cg_indent_base::operator();
    using cg_indent_base::set_level;
    cg_indent& operator++() // prefix increment
    {
        // actual increment takes place here
        ++lvl;
        calc();
        return *this; // return new value by reference
    }
    cg_indent& operator--() // prefix decrement
    {
        // actual decrement takes place here
        --lvl;
        calc();
        return *this; // return new value by reference
    }
};

struct cg_scoped_indent //!< code-gen helper object, scoped indentation shifter
{
    cg_scoped_indent(cg_indent &_cgi, int _shift = 1)
    : cgi(_cgi), shift(_shift)
    {
        doit();
    }
    ~cg_scoped_indent()
    {
        shift = -shift;
        doit();
    }
    private:
    void doit()
    {
        int i = shift;
        while (i > 0) { ++cgi; --i; }
        while (i < 0) { --cgi; ++i; }
    }
    cg_indent &cgi;
    int shift;
};

struct cg_scope_m //!< code-gen helper object for generating scoped blocks
{
    using string = std::string;
    cg_scope_m(
             std::ostream& _os,
             cg_indent& _cgi,
             const string _name = "",
             const string _obrace = "{",
             const string _cbrace = "};"
            )
    : cgi(_cgi), os(_os), name(_name), obr(_obrace), cbr(_cbrace), init(false)
    { }
    ~cg_scope_m()
    {
        if (init == true) { throw std::logic_error("cg_scope_m destructed before being closed.\n"); }
        //close();
    }
    bool open(const std::string _name)
    {
        if (init == true) { return false; }
        name = _name;
        return open();
    }
    bool open()
    {
        if (init == true) { return false; }
        init = true;
        /*
            name_goes_here
            {
                |
                |
                |
            };
        */
        // if the name is empty, don't add a dedicated line for it
        if (!name.empty()) { os << cgi() << name << "\n"; }
        os << cgi() << obr << "\n";
        ++cgi;
        return true;
    }
    bool close()
    {
        if (init == false) { return false; }
        init = false;
        --cgi;
        os << cgi() << cbr << "\n";
        return true;
    }
    private:
    cg_indent& cgi;
    std::ostream& os;
    string name, obr, cbr;
    bool init;
};

struct cg_scope //!< code-gen helper object for generating scoped blocks
{
    using string = std::string;
    cg_scope(
             std::ostream& _os,
             cg_indent& _cgi,
             const string _name = "",
             const string _obrace = "{",
             const string _cbrace = "};"
            )
    : cgi(_cgi), os(_os), name(_name), obr(_obrace), cbr(_cbrace)
    {
        /*
            name_goes_here
            {
                |
                |
                |
            };
        */
        // if the name is empty, don't add a dedicated line for it
        if (!name.empty()) { os << cgi() << name << "\n"; }
        os << cgi() << obr << "\n";
        ++cgi;
    }
    ~cg_scope()
    {
        --cgi;
        os << cgi() << cbr << "\n";
    }
    private:
    cg_indent& cgi;
    std::ostream& os;
    string name, obr, cbr;
};

static void cg_indent_ss(const cg_indent& cgi, std::ostream& os, std::istream& src, const std::string prepend = "") //!< indent a stringstream
{
    std::string line;
    while (std::getline(src, line))
    {
        os << cgi() << prepend << line << "\n";
    }
}

struct cg_pp_if //!< code-gen helper object for generating scoped preprocessor #if ... #endif type of things
{
    using string = std::string;
    cg_pp_if(
             std::ostream& _os,
             cg_indent& _cgi,
             const string _open  = "#if",
             const string _name  = "",
             const string _close = "#endif"
            )
    : cgi(_cgi), os(_os), name(_name), obr(_open), cbr(_close)
    {
        /*
            #if NAME
            |
            |
            |
            #endif // NAME
        */
        os << cgi() << obr;
        if (!name.empty()) { os << " " << name; }
        os << "\n";
    }
    ~cg_pp_if()
    {
        os << cgi() << cbr;
        if (!name.empty()) { os << " // " << name; }
        os << "\n";
    }

    private:
    cg_indent& cgi;
    std::ostream& os;
    string name, obr, cbr;
};

struct z_logger
{
    bool open(const std::string& fn) { of.open(fn); return of.is_open(); }
    template<class... Ts> void operator() (const Ts& ... args)
    {
        if (of.is_open())
        {
            (of << ... << args);
            (std::cout << ... << args);
            if (of.tellp() > 1024*1024*4)
            {
                std::cerr << " * CLOSING LOG FILE * ";
                of.close();
            }
        }
    }

    std::ofstream of;
};

template<typename B, typename T>
void baseclass_copy(T& dst, const T& src)
{
    static_cast<B&>(dst) = static_cast<B const&>(src);
}

//using std::string, std::vector, std::size_t;


#endif // UTIL_HPP_INCLUDED
