#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <unordered_map>

#include "util.hpp"
#include "svd_stuff.hpp"

using namespace tinyxml2;
using std::cout, std::cerr, std::ofstream;
using std::string, std::to_string, std::size_t, std::vector;
//using tinyxml2::XMLDocument, tinyxml2::XMLError, tinyxml2::XMLNode, tinyxml2::XMLElement, tinyxml2::XMLAttribute;
//using namespace tinyxml2::XMLError;
//using namespace std::string_literals;



//bool get_attribute2(const tinyxml2::XMLAttribute *a)

#if 0


TC1_tt.ctrl

//get_attr
#endif // 0

/*
    (from xmega)
    avr-tools-device-file
        variants
        devices
            address-space
            address-spaces (same5x)
            peripherals
            interrupts
            events
            interfaces
            property-groups
        modules
            module
                register-group
                    register
                        bitfield
                    register-group  (again, see USB_EP_TABLE, ADC_CH, structure as member)
                    mode            (on same5x, like I2C/SPI/USART modes for SERCOM)
                value-group
                    value
                interrupt-group
                parameters
                    param
        pinouts


    MODULES seems to contain descriptions of structures (like PORT_t)
    it also apears to have definitions for varios settings and bitmasks
    REGISTER-GROUP can be a structure for a MODULE (like PORT_t)
    VALUE-GROUP can be an enum with constants
    it appears that the original headers are generated with some special code for certain things
    for example on some 16bit registers, (USB endpoint config pointer) the structure is declared
    with a _WORDREGISTER() macro which makes a union between a 16bit register and two 8bit
    registers for the high and low bytes, and this does not appear to be indicated in the ATDF
    so a module parser should have a mechanism to add special handling for certain modules
    a map could be made with each module name linking to the object that has parsed its data


    DEVICES among other things describes instances of MODULES at given addresses
    with given names (like PORTA, PORTB, ...)
*/


//z_logger logger;





//struct member_bucket_t;
//static member_bucket_t g_member_bucket;


//svd_regPropGroup_t.
//svd_dimEG_t


member_ptr_t::member_ptr_t(vector<reg_t> &rv)
{
    mtype = MT_REG;
    idx = rv.size();
    rv.emplace_back();
    v = &rv;
}
member_ptr_t::member_ptr_t(vector<cluster_t> &rv)
{
    mtype = MT_CLUSTER;
    idx = rv.size();
    rv.emplace_back();
    v = &rv;
}


#if 0
member_ptr_t::member_ptr_t(const MEMBER_TYPE mt, member_bucket_t &mb)
{
    switch (mt)
    {
        case MT_REG:
            mtype = mt;
            v = &mb.v_reg;
            idx = mb.v_reg.size();
            mb.v_reg.emplace_back();
            return;

        case MT_CLUSTER:
            mtype = mt;
            v = &mb.v_cluster;
            idx = mb.v_cluster.size();
            mb.v_cluster.emplace_back();
            return;

        case MT_NONE:
            break;
    };
    throw std::logic_error("dafuq are you doing?");
}
#endif // 0

#ifdef OLD_REGZ // gonna make the periph_t contain a main cluster instead of this
struct registers_t
{
    // can contain only reg_t or cluster_t elements
    PARSER_RETCODE parse(const svd_element_t& root_svde, member_bucket_t &mb);

    svd_regPropG_t  regProp; // should be inherited from previous level!
    vector<member_ptr_t>    v_member;
};
#endif // OLD_REGZ





string member_ptr_t::get_name() const
{
    string res = "<%bad_member%>";
    if (v != nullptr)
    {
        if (mtype == MT_REG)
        {
            vector<reg_t> &vv = *reinterpret_cast<vector<reg_t>*>(v);
            res = vv.at(idx).name;
            return res;
        }
        if (mtype == MT_CLUSTER)
        {
            vector<cluster_t> &vv = *reinterpret_cast<vector<cluster_t>*>(v);
            res = vv.at(idx).name;
            return res;
        }
    }
    cerr << "W: get_name() on bad member_ptr_t.\n";
    return res;
}

template<typename B, typename T>
T* derive_element(T& target, const string from, vector<T> &vvv, const string info_type)
{
    T *ppp = nullptr;
    target.is_clone = true;
    target.derivedFrom = from;
    bool found = false;
    // perhaps, there could be a method get_name() for periph_t, field_t and member_ptr_t

    for (T &iii : vvv)
    {
        ppp = &iii;
        if (str_compare(ppp->get_name(), from))
        {
            logger("/// Cloning ", info_type, ": ", from, "\n");
            found = true;
            target.name = ""; // intentionally make this invalid?
            //per = ppp;
            //baseclass_copy<B>(target, *ppp);
            target.derive(*ppp);
            break;
        }
    }
    if (!found)
    {
        ppp = nullptr;
        logger("/// Error: couldn't find reference ", info_type, " ", from, " for cloning.\n");
    }
    return ppp;
}


PARSER_RETCODE SVD_PARSER::parse(const XMLElement *root)
{
    if (root == nullptr) { return RC_ERROR_NULLPTR; }
    PARSER_RETCODE res = RC_SUCCESS;
    svd_element_t root_svde;
    svd_element_t svde1;
    if (!root_svde.read(root)) { return RC_ERROR_NULLPTR; } // TODO: fix this error code
    svde = root_svde;
    res = svde.go_in_and_read(nullptr, "dvc");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_SVD_ELEMENT; }
    //svde.print(logger.of);

    //cout << e->FirstChild().Value() << "\n";
    //const XMLElement *e_periphs = nullptr;

    {
        // scan level 0
        //const XMLElement *e0 = root->FirstChildElement();
        //while (e0 != nullptr)
        while (true)
        {
            //if (!svde.read(e0)) { return RC_ERROR_SVD_ELEMENT; }
            svde.print(logger.of);
            fumber64_t tmp;

            if      (svde.get("size",           regProp.size)) {}
            else if (svde.get("access",         regProp.access)) {}
            else if (svde.get("resetValue",     regProp.resetValue)) {}
            else if (svde.get("resetMask",      regProp.resetMask)) {}

            else if (svde.get("name",           dvc.name)) { header_fnb = dvc.name; str_lower(header_fnb); }
            else if (svde.get("description",    dvc.desc)) {}
            else if (svde.get("series",         dvc.series)) {}
            else if (svde.get("version",        dvc.version)) {}
            else if (svde.get("vendor",         dvc.vendor)) {}
            else if (svde.get("vendorID",       dvc.vendorID)) {}
            else if (svde.get("licenseText",    dvc.licenseText)) {}

            else if (svde.get("addressUnitBits",    dvc.addrUnitBits))
            {
                 if (dvc.addrUnitBits != 8) { logger("* Warning: device:addressUnitBits expected 8, got: ", dvc.addrUnitBits.str(), "\n"); }
            }
            else if (svde.get("width",              dvc.width))
            {
                 if (dvc.width != 32) { logger("* Warning: device:width expected 32, got: ", dvc.width.str(), "\n"); }
            }
            //else if (svde.get("", regProp)) {}
            else
            {
                // elements which should have children instead of text
                if (svde.get("peripherals"))
                {
                    //svde. e_periphs = e0;
                    svde1 = svde; res = svde1.go_in_and_read(nullptr, "prphs");
                    if (res == RC_SUCCESS)
                    {
                        // peripheral
                        while (true)
                        {
                            svde1.print(logger.of);
                            if (svde1.get("peripheral"))
                            {
                                periph_t per;
                                periph_t *ppp = nullptr;
                                per.regs.regProp = regProp;
                                if (svde1.has_attr)
                                {
                                    for (const auto &aaa : svde1.v_attr)
                                    {
                                        if (str_compare(aaa.aname, "derivedFrom"))
                                        {
                                            #if 0
                                            per.is_clone = true;
                                            per.derivedFrom = aaa.aval;
                                            bool found = false;
                                            for (periph_t &iii : v_periph)
                                            {
                                                ppp = &iii;
                                                if (str_compare(ppp->name, aaa.aval))
                                                {
                                                    logger("/// Cloning peripheral: ", aaa.aval, "\n");
                                                    found = true;
                                                    per.name = ""; // intentionally make this invalid?
                                                    //per = ppp;
                                                    //static_cast<periph_inh_t&>(per) = static_cast<periph_inh_t const&>(ppp);
                                                    baseclass_copy<inh_periph_t>(per, *ppp);
                                                    break;
                                                }
                                            }
                                            if (!found)
                                            {
                                                ppp = nullptr;
                                                logger("/// Error: couldn't find reference peripheral ", aaa.aval, " for cloning.\n");
                                            }
                                            #endif // 0
                                            ppp = derive_element<inh_periph_t>(per, aaa.aval, v_periph, "peripheral");
                                        }
                                        else
                                        {
                                            logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", svde1.path, "\n");
                                        }
                                    }
                                }
                                {
                                    PARSER_RETCODE res1 = per.parse(svde1, m_bucket);
                                    if (res1 != RC_SKIP)
                                    {
                                        if (per.is_clone)
                                        {
                                            if (ppp != nullptr)
                                            {
                                                ++ppp->cloned;
                                            }
                                        }
                                        if (res1 != RC_SUCCESS) { return res1; }
                                        v_periph.push_back(per);
                                    }
                                    else
                                    {
                                        logger("// Skipping ", per.name, "\n");
                                    }
                                }
                            }
                            res = svde1.go_down_and_read();
                            if (res == RC_NO_NEXT) { break; }
                        }
                    }
                    else { return RC_ERROR_EMPTY_PERIPHERALS; }
                }
                else if (svde.get("cpu"))
                {
                    PARSER_RETCODE res2 = dvc.cpu.parse(svde, m_bucket);
                    if (res2 != RC_SUCCESS) { return res2; }
                }
                else
                {
                    svde.print_w_unhandled(cout);
                    //cout << "Warning: unhandled element: " << svde.ename << "\n";
                }
            }
            res = svde.go_down_and_read();
            if (res == RC_NO_NEXT) { break; }
            //e0 = e0->NextSiblingElement();
        }
        //logger("sz ", regProp.size.str(), " resetV ", regProp.resetValue.str(), "\n");
    }

    // licenseText is allowed to have "\n" which shall be replaced with actual newlines
    str_replace_all(dvc.licenseText, "\\n", "\n");
    // the following fields are required:
    bool okay = true;
    if (dvc.name.empty())       { okay = false; cout << "Error: device:name is required.\n"; }
    if (dvc.desc.empty())       { okay = false; cout << "Error: device:description is required.\n"; }
    if (dvc.version.empty())    { okay = false; cout << "Error: device:version is required.\n"; }
    if (!dvc.addrUnitBits.good())   { cout << "Warning: device:addressUnitBits is required. Assuming 8.\n"; }
    if (!dvc.width.good())          { cout << "Warning: device:width is required. Assuming 32.\n"; }

    if (!okay) { return RC_ERROR_SVD_ELEMENT; }

    //parsenprint(root, 0); return RC_SUCCESS;
    return RC_SUCCESS;
}
PARSER_RETCODE cpu_t::parse(const svd_element_t& root_svde, member_bucket_t& mb)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read();
    if (res == RC_NO_CHILDREN) { return RC_ERROR_SVD_ELEMENT; }

    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("name",               name)) {}
        else if (svde.get("revision",           revision)) {}
        else if (svde.get("endian",             endian)) {}
        else if (svde.get("mpuPresent",         mpuPresent)) {}
        else if (svde.get("fpuPresent",         fpuPresent)) {}
        else if (svde.get("nvicPrioBits",       nvicPrioBits)) {}
        else if (svde.get("vendorSystickConfig",vendorSystickConfig)) {}

        else if (svde.get("fpuDP",              fpuDP)) {}
        else if (svde.get("dspPresent",         dspPresent)) {}
        else if (svde.get("icachePresent",      icachePresent)) {}
        else if (svde.get("dcachePresent",      dcachePresent)) {}
        else if (svde.get("itcmPresent",        itcmPresent)) {}
        else if (svde.get("dtcmPresent",        dtcmPresent)) {}
        else if (svde.get("vtorPresent",        vtorPresent)) {}
        else if (svde.get("deviceNumInterrupts",deviceNumInterrupts)) {}
        else if (svde.get("sauNumRegions",      sauNumRegions)) {}
        //else if (svde.get("", )) {}
        else
        {
            //elements which don't have text but should have children
            //cout << "* Unhandled element: " << svde.ename << "\n";
            svde.print_w_unhandled(cout);
        }
        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    bool okay = true;
    if (name.empty())                   { okay = false; cout << "Error: cpu:name is required.\n"; }
    if (revision.empty())               { okay = false; cout << "Error: cpu:revision is required.\n"; }
    if (endian.empty())                 { okay = false; cout << "Error: cpu:endian is required.\n"; }
    if (!mpuPresent.good())             { okay = false; cout << "Error: cpu:mpuPresent couldn't be parsed.\n"; }
    if (!fpuPresent.good())             { okay = false; cout << "Error: cpu:fpuPresent couldn't be parsed.\n"; }
    if (!nvicPrioBits.good())           { okay = false; cout << "Error: cpu:nvicPrioBits couldn't be parsed.\n"; }
    if (!vendorSystickConfig.good())    { okay = false; cout << "Error: cpu:vendorSystickConfig couldn't be parsed.\n"; }

    if (!okay) { return RC_ERROR_SVD_ELEMENT; }

    if      (name == "CM0")     { _cpu_id = svd_cpu::CM0; }
    else if (name == "CM0PLUS") { _cpu_id = svd_cpu::CM0PLUS; }
    else if (name == "CM0+")    { _cpu_id = svd_cpu::CM0PLUS; }
    else if (name == "CM1")     { _cpu_id = svd_cpu::CM1; }
    else if (name == "SC000")   { _cpu_id = svd_cpu::SC000; }
    else if (name == "CM23")    { _cpu_id = svd_cpu::CM23; }
    else if (name == "CM3")     { _cpu_id = svd_cpu::CM3; }
    else if (name == "CM33")    { _cpu_id = svd_cpu::CM33; }
    else if (name == "CM35P")   { _cpu_id = svd_cpu::CM35P; }
    else if (name == "CM55")    { _cpu_id = svd_cpu::CM55; }
    else if (name == "SC300")   { _cpu_id = svd_cpu::SC300; }
    else if (name == "CM4")     { _cpu_id = svd_cpu::CM4; }
    else if (name == "CM7")     { _cpu_id = svd_cpu::CM7; }
    else if (name == "CA5")     { _cpu_id = svd_cpu::CA5; }
    else if (name == "CA7")     { _cpu_id = svd_cpu::CA7; }
    else if (name == "CA8")     { _cpu_id = svd_cpu::CA8; }
    else if (name == "CA9")     { _cpu_id = svd_cpu::CA9; }
    else if (name == "CA15")    { _cpu_id = svd_cpu::CA15; }
    else if (name == "CA17")    { _cpu_id = svd_cpu::CA17; }
    else if (name == "CA53")    { _cpu_id = svd_cpu::CA53; }
    else if (name == "CA57")    { _cpu_id = svd_cpu::CA57; }
    else if (name == "CA72")    { _cpu_id = svd_cpu::CA72; }
    else
    {
        cout << "Error: unknown cpu:name(" << name << ").\n";
        return RC_ERROR_SVD_ELEMENT;
    }
    cout << "cpu detected as: " << svd_cpu_name[_cpu_id] << " ::: " << svd_cpu_name2[_cpu_id] << "\n";

    return RC_SUCCESS;
}
PARSER_RETCODE SVD_PARSER::parsenprint(const XMLElement *root, const size_t start_level)
{
    // print the children and their children..
    if (root == nullptr) { return RC_ERROR_NULLPTR; }
    // print current element
    // try to go right -> print it
    // - otherwise go down -> print it
    // - otherwise go back and down -> print it
    // - otherwise - done?
    const XMLElement *e0 = root->FirstChildElement();
    const XMLElement *ex = root->LastChildElement();
    size_t level = start_level;
    size_t peak = level;
    std::map<string, size_t> stats1;
    while (e0 != nullptr)
    {
        if (!svde.read(e0)) { logger("Err: ", e0->GetLineNum(), " : ", svde.ename, "\n"); }
        svde.level = level; // HACK HACK
        svde.print(logger.of);

        {
            // for statistics
            string tmp = svde.ename + ':' + std::to_string(level);
            if (svde.has_attr)  { tmp += ":attr"; }
            else                { tmp += ":    "; }
            stats1[tmp] = stats1[tmp] + 1;
        }
        if (level > peak) { peak = level; }

        const XMLElement *e1 = e0;
        // go right
        e0 = e1->FirstChildElement();
        if (e0 != nullptr) { ++level; continue; }
        // or down
        else { e0 = e1->NextSiblingElement(); }
        // or back
        while (e0 == nullptr)
        {
            // if the element was the last child of the root node don't go back, exit
            if (e1 == ex) { break; }

            if (e1->Parent() == nullptr) { break; }

            e0 = e1->Parent()->ToElement();
            --level;
            // if we reach the root element - stop? .. actually no
            //if (e0 == root) { e0 = nullptr }
            e1 = e0;
            e0 = e1->NextSiblingElement();
            // now there may be no next sibling, so go back even moar
        }
        if (e0 == nullptr) { break; }
    }
    logger.of << "----------------\n";
    logger.of << "parsenprint peak level: " << peak << "\n\n";
    for (const auto &i : stats1)
    {
        logger.of << "// " << (i.first) << " -----> " << (i.second) << "\n";
    }
    return RC_SUCCESS;
}

bool SVD_PARSER::memb_check_sizes(vector<member_ptr_t> &mpv)
{
    /*
        regs
            cluster
                reg
                reg
                reg
            cluster
                cluster
                    reg
                    reg
                reg
                reg
                reg
    */
    //size_t i =
    //mpv.size()
    return true;
}

PARSER_RETCODE svd_addrBlock_t::parse(const svd_element_t& root_svde)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read();
    if (res == RC_NO_CHILDREN) { return RC_ERROR_ADDR_BLOCK; }
    //svde.print(logger.of);

    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("offset",     offset)) {}
        else if (svde.get("size",       size)) {}
        else if (svde.get("usage",      usage)) {}
        else if (svde.get("protection", protection)) {}
        //else if (svde.get("", )) {}
        else
        {
            //elements which don't have text but should have children
            //cout << "* Unhandled element: " << svde.ename << "\n";
            svde.print_w_unhandled(cout);
        }
        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    return RC_SUCCESS;
}

PARSER_RETCODE periph_t::parse(const svd_element_t& root_svde, member_bucket_t &mb)
{
    svd_element_t svde = root_svde;

    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "per:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }
    //svde.print(logger.of);

    //while (e != nullptr)
    while (true)
    {
        // parse the known/expected things first
        svd_regPropG_t &regProp = regs.regProp;
        svde.print(logger.of);
        if      (svde.get("name",           name))
        {
            svde.path += name;
            #if 0
            // TEMPORARY:
            if      (str_compare(name, "ADC0")) {}
            else if (str_compare(name, "ADC1")) {}
            else if (str_compare(name, "AES")) {}
            else if (str_compare(name, "SERCOM0")) {}
            else { return RC_SKIP; }
            #elif 0
            if      (str_compare(name, "I2S")) {}
            else { return RC_SKIP; }
            #endif // 0
        }
        else if (svde.get("description",            desc)) {}
        else if (svde.get("baseAddress",            baseAddr)) {}
        else if (svde.get("version",                version)) {}
        else if (svde.get("appendToName",           appendToName)) {}
        else if (svde.get("prependToName",          prependToName)) {}
        else if (svde.get("groupName",              groupName)) {}
        else if (svde.get("alternatePeripheral",    alternatePeripheral)) {}
        else if (svde.get("headerStructName",       headerStructName)) {}

        else if (svde.get("dim",            dimEG.dim)) {}
        else if (svde.get("dimIncrement",   dimEG.dimIncrement)) {}
        else if (svde.get("dimName",        dimEG.dimName)) {}
        else if (svde.get("dimIndex",       dimEG.dimIndex)) {}

        else if (svde.get("size",       regProp.size)) {}
        else if (svde.get("access",     regProp.access)) {}
        else if (svde.get("resetValue", regProp.resetValue)) {}
        else if (svde.get("resetMask",  regProp.resetMask)) {}
        else if (svde.get("protection", regProp.protection)) {}
        //else if (svde.get("", per)) {}
        else
        {
            //elements which don't have text but should have children
            if (svde.get("addressBlock"))
            {
                // there can be zero or many addressBlocks
                svd_addrBlock_t ab;
                PARSER_RETCODE res = ab.parse(svde);
                if (res != RC_SUCCESS) { return res; }
                v_addrBlock.push_back(ab);
            }
            else if (svde.get("registers"))
            {
                res = regs.parse(svde, mb);
                if (res != RC_SUCCESS) { return res; }
            }
            else if (svde.get("interrupt"))
            {
                interrupt_t irp;
                res = parse_interrupt(svde, irp);
                if (res != RC_SUCCESS) { return res; }
                v_int.emplace_back(irp);
                mb.v_int.emplace_back(irp);
            }
            else
            {
                svde.print_w_unhandled(cout);
            }
        }
        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    if (dimEG.dim.good()) { logger.of << "/// Need to fiddle with dimEG for " << svde.path << "!\n"; }
    if (!prependToName.empty()) { logger.of << "/// prependToName used " << svde.path << "!\n"; }
    if (!alternatePeripheral.empty()) { logger.of << "/// alternatePeripheral used " << svde.path << "!\n"; }
    //if (!groupName.empty()) { logger.of << "/// groupName used " << svde.path << "!\n"; }

    return RC_SUCCESS;
}

PARSER_RETCODE periph_t::parse_interrupt(const svd_element_t& root_svde, interrupt_t& irp)
{
    svd_element_t svde = root_svde;

    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "per:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }
    //svde.print(logger.of);

    //while (e != nullptr)
    while (true)
    {
        // parse the known/expected things first
        svd_regPropG_t &regProp = regs.regProp;
        svde.print(logger.of);
        if      (svde.get("name",           irp.name)) {}
        else if (svde.get("description",    irp.desc)) {}
        else if (svde.get("value",          irp.value)) {}
        else
        {
            svde.print_w_unhandled(cout);
        }
        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    if (irp.name.empty())
    {
        logger.of << "/// interrupt:name is required.\n";
        return RC_ERROR_SVD_ELEMENT;
    }
    if (!irp.value.good())
    {
        logger.of << "/// interrupt:value couldn't be parsed.\n";
        return RC_ERROR_SVD_ELEMENT;
    }
    return RC_SUCCESS;
}


#if 0
PARSER_RETCODE registers_t::parse(const svd_element_t& root_svde, member_bucket_t &mb)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "regs");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    while (true)
    {
        svde.print(logger.of);
        if      (svde.get("register"))
        {
            reg_t reg;

            res = reg.parse(svde);
            if (res != RC_SUCCESS) { return res; }
            v_member.emplace_back(reg, mb.v_reg);
        }
        else if (svde.get("cluster"))
        {
            cluster_t clu;
            //clu.regProp <-------- fix this..?!?!
            cluster_t *ppp = nullptr;
            {
                for (const auto &aaa : svde.v_attr)
                {
                    if (str_compare(aaa.aname, "derivedFrom"))
                    {
                        // cast the v_member<> to type member_t???
                        // or maybe another option is to make a .derive() method
                        // in member_ptr_t, periph_t, field_t
                        //ppp = derive_element<member_t>(clu, aaa.aval, v_member, "cluster");
                        logger("/// TODO: Need to derive a cluster from ", aaa.aval, "\n");
                        throw std::logic_error("derived clusters not implemented\n");
                    }
                    else
                    {
                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", svde.path, "\n");
                    }
                }
            }
            res = clu.parse(svde, mb);
            if (res != RC_SUCCESS) { return res; }
            //member_ptr_t memb(mb.v_cluster);
            //*memb.get_cluster() = clu;
            //v_member.push_back(memb);

            v_member.emplace_back(clu, mb.v_cluster);
        }
        else
        {
            svde.print_w_unhandled(cout);
        }

        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    return RC_SUCCESS;
}
#endif // 0

PARSER_RETCODE cluster_t::parse(const svd_element_t& root_svde, member_bucket_t &mb)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "clu:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("name",               name)) { svde.path += name; raw_name = name; }
        else if (svde.get("description",        desc)) {}
        else if (svde.get("addressOffset",      addr_offs)) {}
        else if (svde.get("headerStructName",   headerStructName)) {}
        else if (svde.get("alternateCluster",   altCluster)) {}

        else if (svde.get("dim",            dimEG.dim)) {}
        else if (svde.get("dimIncrement",   dimEG.dimIncrement)) {}
        else if (svde.get("dimName",        dimEG.dimName)) {}
        else if (svde.get("dimIndex",       dimEG.dimIndex)) {}

        else if (svde.get("size",           regProp.size)) {}
        else if (svde.get("access",         regProp.access)) {}
        else if (svde.get("resetValue",     regProp.resetValue)) {}
        else if (svde.get("resetMask",      regProp.resetMask)) {}
        //else if (svde.get("", per)) {}
        else
        {
            //elements which don't have text but should have children
            if (svde.get("register"))
            {
                reg_t reg;

                res = reg.parse(svde);
                if (res != RC_SUCCESS) { return res; }
                v_member.emplace_back(reg, mb.v_reg);
            }
            else if (svde.get("cluster"))
            {
                // nested clusters ...
                // perhaps there should be a limit for teh deepness?
                logger.of << "/// Nested clusters: " << svde.path << "!\n";
                cluster_t clu;
                //clu.regProp
                res = clu.parse(svde, mb);
                if (res != RC_SUCCESS) { return res; }

                v_member.emplace_back(clu, mb.v_cluster);
            }
            else
            {
                svde.print_w_unhandled(cout);
            }
        }
        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    if (dimEG.dim.good()) { logger.of << "/// Need to fiddle with dimEG for " << svde.path << "!\n"; }
    if (!altCluster.empty()) { logger.of << "/// alternateCluster used " << svde.path << "!\n"; }
    if (!headerStructName.empty()) { logger.of << "/// headerStructName used " << svde.path << "!\n"; }
    return RC_SUCCESS;
}
PARSER_RETCODE reg_t::parse(const svd_element_t& root_svde)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "reg:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("name",               name)) { svde.path += name; raw_name = name; }
        else if (svde.get("description",        desc)) {}

        else if (svde.get("displayName",        displayName)) {}
        else if (svde.get("dataType",           dataType)) {}
        else if (svde.get("addressOffset",      addr_offs)) {}
        else if (svde.get("alternateRegister",  altReg)) {}
        else if (svde.get("alternateGroup",     altGroup)) {}

        else if (svde.get("dim",            dimEG.dim)) {}
        else if (svde.get("dimIncrement",   dimEG.dimIncrement)) {}
        else if (svde.get("dimName",        dimEG.dimName)) {}
        else if (svde.get("dimIndex",       dimEG.dimIndex)) {}

        else if (svde.get("size",           regProp.size)) {}
        else if (svde.get("access",         regProp.access)) {}
        else if (svde.get("resetValue",     regProp.resetValue)) {}
        else if (svde.get("resetMask",      regProp.resetMask)) {}

        else if (svde.get("fields"))
        {
            // todo: fields
            svd_element_t svde1 = svde;
            res = svde1.go_in_and_read(nullptr, "flds:");
            if (res == RC_NO_CHILDREN) { return RC_NO_CHILDREN; }
            while (true)
            {
                svde1.print(logger.of);
                if (svde1.get("field"))
                {
                    field_t fld;
                    res = fld.parse(svde1);
                    if (res != RC_SUCCESS) { return res; }
                    v_field.emplace_back(fld);
                }
                else
                {
                    svde1.print_w_unhandled(cout);
                }

                res = svde1.go_down_and_read();
                if (res == RC_NO_NEXT) { break; }
            }
        }
        else
        {
            svde.print_w_unhandled(cout);
        }

        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    if (dimEG.dim.good()) { logger.of << "/// Need to fiddle with dimEG for " << svde.path << "!\n"; }
    if (!altReg.empty()) { logger.of << "/// alternateRegister used " << svde.path << "!\n"; }
    if (!altGroup.empty()) { logger.of << "/// alternateGroup used " << svde.path << "!\n"; }
    if (!regProp.size.good()) { cout << "/// Need to inherit register size!\n"; }

    if (!addr_offs.good()) { return RC_ERROR_SVD_ELEMENT; }
    return RC_SUCCESS;
}

PARSER_RETCODE field_t::parse(const svd_element_t& root_svde)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "fld:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    uint16_t br_spec = 0; //
    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("name",               name)) { svde.path += name; }
        else if (svde.get("description",        desc)) {}
        else if (svde.get("bitOffset",          bitOffset)) { br_spec |= 0x0001; }
        else if (svde.get("bitWidth",           bitWidth))  { br_spec |= 0x0002; }
        else if (svde.get("lsb",                lsb))       { br_spec |= 0x0010; }
        else if (svde.get("msb",                msb))       { br_spec |= 0x0020; }
        else if (svde.get("bitRange",           bitRange))  { br_spec |= 0x0300; }
        else if (svde.get("access",             access)) {}

        else if (svde.get("dim",            dimEG.dim)) {}
        else if (svde.get("dimIncrement",   dimEG.dimIncrement)) {}
        else if (svde.get("dimName",        dimEG.dimName)) {}
        else if (svde.get("dimIndex",       dimEG.dimIndex)) {}

        else if (svde.get("enumeratedValues"))
        {
            /*
            svd_element_t svde1 = svde;
            res = svde1.go_in_and_read(nullptr, "enum:");
            if (res == RC_NO_CHILDREN) { return RC_NO_CHILDREN; }
            while (true)
            {
                if      (svde1.get("enumeratedValue"))
                {
                    field_t fld;
                    res = fld.parse(svde1);
                    if (res != RC_SUCCESS) { return res; }
                    v_field.emplace_back(fld);
                }
                else if (svde1.get("name", this.))
                else
                {
                    svde1.print_w_unhandled(cout);
                }

                res = svde1.go_down_and_read();
                if (res == RC_NO_NEXT) { break; }
            }
            */
            res = enums.parse(svde);
            if (res != RC_SUCCESS) { return res; }
        }
        else
        {
            svde.print_w_unhandled(cout);
        }

        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }
    if (dimEG.dim.good()) { logger.of << "/// Need to fiddle with dimEG for " << svde.path << "!\n"; }

    // deal with the bitrange
    if      (br_spec == 0x0003)
    {
        lsb = bitOffset;
        msb = lsb; msb.val += bitWidth.val - 1;
        bitRange = "[" + msb.str() + ":" + lsb.str() + "]";
    }
    else if (br_spec == 0x0030)
    {
        bitOffset = lsb;
        bitWidth = msb; bitWidth.val = 1 + bitWidth.val - lsb.val;
        bitRange = "[" + msb.str() + ":" + lsb.str() + "]";
    }
    else if (br_spec == 0x0300)
    {
        bool okay = false;
        if (bitRange.length() >= 5)
        {
            const size_t n = bitRange.length();
            if ((bitRange[0] == '[') && (bitRange[n-1] == ']'))
            {
                const size_t pos0 = bitRange.find(':', 1);
                if (pos0 != std::string::npos)
                {
                    string tmp0 = bitRange.substr(1,pos0-1);
                    string tmp1 = bitRange.substr(pos0+1, n-pos0-2);
                    if (msb.set(tmp0) && (lsb.set(tmp1)))
                    {
                        okay = true;
                    }
                }
            }
        }
        if (!okay)
        {
            cout << "Error: couldn't parse bitRange string: \"" << bitRange << "\"\n";
            return RC_ERROR_SVD_ELEMENT;
        }

        bitOffset = lsb;
        bitWidth = msb; bitWidth.val = 1 + bitWidth.val - lsb.val;
        bitRange = "[" + msb.str() + ":" + lsb.str() + "]";
    }

    return RC_SUCCESS;
}

PARSER_RETCODE enums_t::parse(const svd_element_t& root_svde)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "enum:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    if (svde.has_attr)
    {
        for (const auto &aaa : svde.v_attr)
        {
            if (str_compare(aaa.aname, "derivedFrom"))
            {
                // TODO
                //ppp = derive_element<inh_periph_t>(per, aaa.aval, v_periph, "peripheral");
                logger("/// W: TODO: Enum derivedFrom(", aaa.aval, ")!!!\n");
            }
            else
            {
                logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", svde.path, "\n");
            }
        }
    }
    while (true)
    {
        // parse the known/expected things first
        svde.print(logger.of);
        if      (svde.get("name",               name)) { svde.path += name; }
        else if (svde.get("headerEnumName",     headerEnumName)) {}
        else if (svde.get("usage",              usage)) {}

        else if (svde.get("enumeratedValue"))
        {
            enumV_t enm;
            res = enm.parse(svde);
            if (res != RC_SUCCESS) { return res; }
            v_enum.emplace_back(enm);
        }
        else
        {
            svde.print_w_unhandled(cout);
        }

        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }

    return RC_SUCCESS;
}
PARSER_RETCODE enumV_t::parse(const svd_element_t& root_svde)
{
    svd_element_t svde = root_svde;
    PARSER_RETCODE res = svde.go_in_and_read(nullptr, "eval:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_PERIPHERALS; }

    while (true)
    {
        svde.print(logger.of);
        if      (svde.get("name",               name)) { svde.path += name; }
        else if (svde.get("description",        desc)) {}
        else if (svde.get("value",              value)) {}
        else if (svde.get("isDefault",          isDefault)) {}
        else
        {
            svde.print_w_unhandled(cout);
        }

        res = svde.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }

    return RC_SUCCESS;
}

member_t* member_ptr_t::get_member()
{
    // instead of returning reg_t or cluster_t, return the base-class (member_t)
    if (v != nullptr)
    {
        if (mtype == MT_REG)
        {
            vector<reg_t> &vv = *reinterpret_cast<vector<reg_t>*>(v);
            return &vv.at(idx);
        }
        if (mtype == MT_CLUSTER)
        {
            vector<cluster_t> &vv = *reinterpret_cast<vector<cluster_t>*>(v);
            return &vv.at(idx);
        }
    }
    return nullptr;
}

// #########################################################

// #########################################################

// #########################################################

// #########################################################

const string this_version_str = std::string("v0.99 (") + __DATE__ + ")";

int main(int argc, const char **argv)
{
    std::string fnb;


    #ifndef ATDF_NO_ARGS
    if (argc < 3)
    {
        cout << "* Not enough arguments.\n";
        cout << "Usage: <path/to/file.svd> <output_dir/>\n";
        return 0;
    }
    #endif // ATDF_NO_ARGS

    #if 0
    fnb = argv[1];
    std::string outdir = argv[2];
    #else
    #ifdef JJJ
    fnb = R"(D:\antto\atpack\same54\svd\ATSAME54N20A.svd)";
    fnb = R"(D:\antto\ARM\svd\Microchip.SAME54_DFP.3.6.95\svd\ATSAME54N20A.svd)";
    //fnb = R"(D:\antto\ARM\svd\Microchip.SAMD21_DFP.3.5.132\samd21a\svd\ATSAMD21J18A.svd)";
    //fnb = R"(D:\antto\ARM\svd\Atmel.SAMD21_DFP.1.3.395\samd21a\svd\ATSAMD21J18A.svd)"; // OLD?
    //fnb = R"(D:\antto\ARM\svd\Keil.STM32F4xx_DFP.2.15.0\CMSIS\SVD\STM32F46_79x.svd)";
    //fnb = R"(D:\antto\ARM\svd\Keil.STM32L4xx_DFP.2.6.1\CMSIS\SVD\STM32L496.svd)";
    //fnb = R"(D:\antto\ARM\svd\GigaDevice.GD32F4xx_DFP.2.0.0\SVD\GD32F4xx.svd)";
    #else
    //fnb = "/home/h4x0riz3d/ZONE/atmel_atdf/same54/svd/ATSAME54N20A.svd";
    fnb = "/home/h4x0riz3d/ZONE/z_SVD/Microchip.SAMD21_DFP.3.5.132/samd21a/svd/ATSAMD21J18A.svd";
    fnb = "/home/h4x0riz3d/ZONE/z_SVD/Microchip.SAME54_DFP.3.6.95/svd/ATSAME54N20A.svd";
    //fnb = "/home/h4x0riz3d/ZONE/z_SVD/Microchip.SAME70_DFP.4.6.98/same70b/svd/ATSAME70N21B.svd";
    //fnb = "/home/h4x0riz3d/ZONE/z_SVD/STM32F4_svd_V1.2/STM32F469.svd";
    fnb = "/home/h4x0riz3d/ZONE/z_SVD/GigaDevice.GD32F4xx_DFP.2.0.0/SVD/GD32F4xx.svd";
    //fnb = "/home/h4x0riz3d/ZONE/z_SVD/Keil.STM32F4xx_DFP.2.15.0/CMSIS/SVD/STM32F46_79x.svd";
    fnb = "/home/h4x0riz3d/ZONE/z_SVD/Keil.STM32L4xx_DFP.2.6.1/Keil.STM32L4xx_DFP.2.6.1/CMSIS/SVD/STM32L496.svd";
    //fnb = "/home/h4x0riz3d/ZONE/z_SVD/rp2040/rp2040.svd";
    //fnb = "/home/h4x0riz3d/ZONE/z_SVD/fake1.svd";
    #endif // JJJ
    string outdir = "wub/";
    #endif // 0


    if (fnb.length() <= 1) { return 0; }
    cout << "* Input file: \"" << fnb << "\"\n";
    cout << "* Output path: \"" << outdir << "\"\n\n";

    {
        SVD_PARSER svdp;
        svdp.src_fn = fnb;
        string chipname;
        ofstream hpp;
        string fnout;
        string fnlog;// = outdir + "meh.txt"; logger.open(fnlog);
        //vector<parse_module> v_module;
        vector<string> v_skipped_module;
        XMLDocument atdf;
        tinyxml2::XMLError res = atdf.LoadFile(fnb.c_str());
        bool work = true;
        if (res == XML_SUCCESS)
        {
            //
            XMLElement *rootE = atdf.FirstChildElement("device");
            tinyxml2::XMLNode *rootNode = rootE;
            tinyxml2::XMLNode *tmpA; //, *tmpB;

            tmpA = rootNode;
            while (work)
            {
                if (tmpA == nullptr) { break; }
                cout << "BEGIN: " << tmpA->Value() << "\n";
                const tinyxml2::XMLElement *elem0; //, *e_device;
                //e_device = nullptr;
                elem0 = tmpA->FirstChildElement("name");

                //svdp.print_node(tmpA->FirstChild());
                //svdp.print_element(tmpA->FirstChildElement("peripheral"));
                if (elem0 != nullptr)
                {
                    if (get_element_text(elem0, chipname))
                    {
                        //cout << "Found device: " << chipname << "\n";
                        logger("Found device: ", chipname, "\n");
                        string tmp = chipname;
                        str_lower(tmp);
                        fnout = outdir + tmp + ".hpp";
                        fnlog = outdir + tmp + ".log.txt";
                        logger.open(fnlog);
                    }
                    else
                    {
                        cerr << "Error: couldn't find the chip name.\n";
                        return 0;
                    }
                    cout << "Parsing...\n";
                    auto res = svdp.parse(rootE);
                    cout << "Parser returned: " << res << "\n";
                    if (res != RC_SUCCESS) { work = false; }

                    fnout = outdir + svdp.header_fnb + ".hpp";

                    svdp.process();
                    //return 0;
                    break;
                }
            }
            //while ()
            #if 0
            if (work)
            {
                cout << "* Processing...\n";
                for (auto it = v_module.begin(); it != v_module.end(); ++it)
                {
                    it->proc();
                }
            }
            #endif // 0
            if (work)
            {
                cout << "* Opening output file: " << fnout << "\n";
                hpp.open(fnout);
                if (hpp.is_open() == false)
                {
                    cout << "* Error: couldn't open output file for writing\n";
                    work = false;
                }
                else
                {
                    //fnout << ""
                }
            }
            if ((work) && (hpp.is_open()))
            {
                // print the stuff to the file
                cout << "* Writing to file...\n";
                svdp.gen(hpp);
                #if 0
                for (auto it = v_module.begin(); it != v_module.end(); ++it)
                {
                    it->print(hpp);
                }
                #endif // 0
                work = false;
            }
            #if 0
            if (v_skipped_module.size())
            {
                cout << "* Skipped modules(" << v_skipped_module.size() << "):\n";
                for (auto it = v_skipped_module.begin(); it != v_skipped_module.end(); ++it)
                {
                    cout << "  - " << *it << "\n";
                }
            }
            #endif // 0
        }
        else
        {
            // oops, some other error... halt
            cout << "* XML error: " << atdf.ErrorIDToName(res) << "\n* Aborting...\n";
            //abort = true;
        }
        //std::string fnout = outdir +
    }

    return 0;
    // #############################################################################
}
