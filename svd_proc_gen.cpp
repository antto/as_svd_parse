#include "util.hpp"
#include "svd_stuff.hpp"
#include <sstream>

constexpr size_t ntbf = 4;
constexpr size_t wtbf = 80;
static cg_indent cgi;

bool regprop_defaults(const svd_regPropG_t& rpa, svd_regPropG_t& rpb)
{
    bool mod = false; // was anything modified?
    if (rpb.access.empty())             { mod = true; rpb.access        = rpa.access; }
    if (rpb.protection.empty())         { mod = true; rpb.protection    = rpa.protection; }
    if (rpb.size.good()       == false) { mod = true; rpb.size          = rpa.size; }
    if (rpb.resetValue.good() == false) { mod = true; rpb.resetValue    = rpa.resetValue; }
    if (rpb.resetMask.good() ==  false) { mod = true; rpb.resetMask     = rpa.resetMask; }
    return mod;
}

bool compare_interrupt(const interrupt_t& a, const interrupt_t& b)
{
    // smallest comes first
    if (a.value.good() && b.value.good())
    {
        return a.value.val < b.value.val;
    }
    throw std::logic_error("comparing interrupts with bad values.");
}

bool SVD_PARSER::process()
{
    cout << "-- PROCESSING --\n";
    logger("\n");
    logger("// device name: ", dvc.name, "\n");
    logger("// description: ", dvc.desc, "\n");
    logger("// device version: ", dvc.version, "\n");
    logger("// generated filename base (without extension): ", header_fnb, "\n");
    logger("// regProp.size......: ", regProp.size.str_safe(), "\n");
    logger("// regProp.access....: ", regProp.access, "\n");
    logger("// regProp.protection: ", regProp.protection, "\n");
    logger("// regProp.resetValue: ", regProp.resetValue.str_safe(), "\n");
    logger("// regProp.resetMask.: ", regProp.resetMask.str_safe(), "\n");
    logger("Periphs: ", v_periph.size(), "\n");

    size_t n = v_periph.size();
    size_t i = 0;
    while (i < n)
    {
        // for each periph
        periph_t& per = v_periph.at(i);
        if (per.prependToName.empty())
        {
            if (!per.headerStructName.empty())  { per.prependToName = per.headerStructName; }
            else                                { per.prependToName = per.name; }
            str_rem_surrounding_chars(per.prependToName, "_");
            per.prependToName += "_";
        }
        scoped_ipath s_ipath(m_bucket.v_ipath);
        svd_inh_path& ipath = s_ipath.get();
        {
            ipath.name = per.name;
            ipath.varname = per.prependToName;
            if (ipath.varname.empty()) { ipath.varname = per.headerStructName; }
        }
        {
            // if peripheral regProp is empty, copy them from the device
            regprop_defaults(regProp, per.regs.regProp);
        }
        if (per.process(m_bucket) == false) { return false; }
        string tmp = "-";
        if (per.is_clone) { tmp = "* clone of: " + per.derivedFrom; }

        logger("  Per: ", per.name, ", ", per.regs.v_member.size(), " members ", tmp, "\n");
        //size_t i1
        ++i;
    }
    {
        // sort the interrupts
        vector<interrupt_t>& v0 = m_bucket.v_int;
        //vector<interrupt_t> v1;
        std::sort(v0.begin(), v0.end(), compare_interrupt);

        {
            // make the interrupt numbers look nice
            fumber_t<int32_t> tmp;
            tmp.set("-1");
            for (interrupt_t& irp : m_bucket.v_int)
            {
                tmp.grow_style(irp.value);
            }
            tmp.recalc_style();
            for (interrupt_t& irp : m_bucket.v_int)
            {
                irp.value.copy_style(tmp);
            }
        }
    }
    return true;
}

bool periph_t::process(member_bucket_t &mb)
{
    // if headerStructName is not specified - use the name
    // it may clash in some cases like same54: name="AES"
    cout << "process() periph(" << name << ")\n";
    if (headerStructName.empty())
    {
        if (!groupName.empty()) { headerStructName = groupName; } // *HACK* maybe works on same54?
        else                    { headerStructName = name; } // what SVD docs suggest
    }
    if (str_compare(name, headerStructName))
    {
        logger("// W: Peripheral StructName/name conflict: ", name, " ", headerStructName, "\n");
        //headerStructName
        nspace = "peripherals";
    }
    // -------
    // if the peripheral is unique (not a derivedFrom) then it *MUST* have
    // at least one addressBlock
    if (is_clone == false)
    {
        if (v_addrBlock.empty())
        {
            logger("// Error: non-derived peripheral ", name, " does not have an addressBlock\n");
            return false;
        }
        // offset is relative to the peripheral baseAddress
        for (svd_addrBlock_t &ab : v_addrBlock)
        {
            logger("ab.usage ", ab.usage, "\n");
            logger("ab.offset ", ab.offset.val, "\n");
            logger("ab.size ", ab.size.val, "\n");
            logger("ab.prot ", ab.protection, "\n");
        }

        // registers
        /*
            scan the members:
            - process() all the clusters first
            - and each cluster should process its own clusters first
            at some point, the deepest cluster (which has only registers) will be reached
            then it will process its registers, sort all members, sum their sizes
            then the size of the deepest cluster is known and can return back
            to its parent cluster, chich can then process its registers too..

        */
        #if 1
        if (!regs.process(mb)) { return false; }
        if (!regs.finalize(mb)) { return false; }
        #else
        if (!regs.v_member.empty())
        {
            for (member_ptr_t &memb : regs.v_member)
            {
                if (memb.mtype == MT_CLUSTER)
                {
                    cluster_t& clu = *memb.get_cluster();
                    scoped_ipath s_ipath(mb.v_ipath);
                    svd_inh_path& ipath = s_ipath.get();
                    {
                        ipath.name = clu.name;
                        ipath.varname = clu.altCluster; // not sure, testing
                        if (ipath.varname.empty()) { ipath.varname = clu.name; }
                    }
                    regprop_defaults(regs.regProp, clu.regProp);
                    clu.process(mb);
                }
            }
            for (member_ptr_t &memb : regs.v_member)
            {
                if (memb.mtype == MT_REG)
                {
                    reg_t& reg = *memb.get_reg();
                    scoped_ipath s_ipath(mb.v_ipath);
                    svd_inh_path& ipath = s_ipath.get();
                    {
                        ipath.name = reg.name;
                        ipath.varname = reg.altReg; // not sure, testing
                        if (ipath.varname.empty()) { ipath.varname = reg.name; }
                    }
                    regprop_defaults(regs.regProp, reg.regProp);
                    reg.process(mb);
                }
            }
        }
        #endif // 1
    }
    return true;
}

bool make_padding_reg(member_bucket_t &mb, reg_t &reg, const size_t offs, const size_t size)
{
    // TODO: add a flag to reg_t to indicate this is artificially made for padding?
    //cout << __PRETTY_FUNCTION__ << "\n";
    reg.is_padding = true;
    reg.addr_offs.set("0");
    reg.addr_offs = offs;
    reg.name = "_rsvd" + reg.addr_offs.str() + "[%s]";
    reg.raw_name = reg.name;
    reg.desc = "padding";
    reg.regProp.size.set("8");
    // TODO: change regProp.access to read-only?
    //reg.regProp.size = 8;
    if (size == 0)
    {
        throw std::logic_error("Error: dafuq are you padding, boi?\n");
        return false;
    }
    if (size >= 1)
    {
        reg.dimEG.dim.set("0");
        reg.dimEG.dimIncrement.set("1");

        reg.dimEG.dim = size;
        reg.dimEG.dimIncrement = 1;
    }
    //cout << "* Adding padding member: " << reg.name << " size=" << size << "\n";
    return reg.process(mb);
}

template<typename T>
bool adj_array_name(T& target) // (reg_t& target)
{
    // this should be called when the target has been processed
    // specifically the dimEG bits

    // elements which can be made into arrays:
    // periph, reg, cluster, field

    // for arrays: remove the [%s] from the reg/clu.name!

    target.decl_name0 = target.name;
    target.decl_name1 = "";
    if (target.dimEG.dim.good())
    {
        // perhaps also check for max size?
        if (target.dimEG.dim.val >= 1)
        {
            // if the name ends with [%s] it's indeed an array
            // maybe strip it away.. gen() should use a different name that looks like "REG_NAME[ 4]" or so
            //#error "CHECKPOINT!"
            string tmp = target.name;
            if (str_replace(tmp, "[%s]", ""))
            {
                assert(tmp.empty() == false);
                target.name = tmp;
            }
            else
            {
                cout << "Warning: no \"[%s]\" found in array name: " << target.name << "\n";
                //return false;
            }
            target.decl_name0 = target.name + '[';
            target.decl_name1 = ']';
        }
    }
    return true;
}

bool cluster_t::process(member_bucket_t &mb)
{
    //adj_array_name(*this); // hm
    //cout << "process() cluster(" << name << ")\n";
    if (v_member.empty()) { logger("// W: cluster ", name, " has no members.\n"); }

    // process all the clusters first!
    // on the deepest cluster, this for loop will get skipped!
    total_size = 0;
    single_sz = 0;
    ni = 1;
    for (member_ptr_t &memb : v_member)
    {
        if (memb.mtype == MT_CLUSTER)
        {
            cluster_t& clu = *memb.get_cluster();
            regprop_defaults(regProp, clu.regProp);
            clu.process(mb);
        }
    }
    // process the registers now
    size_t rec_sz = 0; // record size
    for (member_ptr_t &memb : v_member)
    {
        if (memb.mtype == MT_REG)
        {
            //memb.get_reg()->process(); <- why was this commented-out?!
            reg_t &reg = *memb.get_reg();
            regprop_defaults(regProp, reg.regProp);
            reg.process(mb); // hm?
            size_t cur_sz = reg.addr_offs + reg.total_size;
            if (rec_sz < cur_sz) { rec_sz = cur_sz; }
        }
    }
    // sort all members now and calculate/check the cluster size?

    // the following section plays the role of calc()
    uint8_t z_array = 0;
    {
        const uint8_t d0 = dimEG.dim.good();
        const uint8_t d1 = dimEG.dimIncrement.good();
        z_array = (d0 ^ d1) | (d0 & d1);
    }
    if (z_array) //(dimEG.dim.good())
    {
        _is_array = true;
        // both dim and dimIncrement have to be valid or not present at all
        // dimEG.dimIncrement mandates a strict size
        // because when you do type[4] each element has to land on the right
        // address
        // so registers with dimEG stuff must use the proper decl_typename
        // clusters must have padding members after the last legit element if
        // their size is otherwise insufficient
        if (rec_sz == dimEG.dimIncrement.val)
        {
            // the calculated size happens to match the element spacing?
            single_sz = rec_sz;
        }
        else if (rec_sz < dimEG.dimIncrement.val)
        {
            // the calculated size is less
            // the cluster needs padding elements on the back
            single_sz = dimEG.dimIncrement.val;
        }
        else
        {
            logger("// Error: cluster array element size is smaller than the calculated size.\n");
            return false;
        }
        ni = dimEG.dim.val;
        if ((ni == 0) || (single_sz == 0))
        {
            logger("// Error: cluster size calculation error, size=", single_sz, "*", ni, "\n");
            return false;
        }
    }
    /*
        v_member is a vector of member_ptr_t
        this vector can be reordered safely
        the vectors in member_bucket_t cannot!

        so, make a temporary v_member-like vector
        scan v_member to find the member starting from the lowest offset
        - if it's above 0 - insert a padding member into the member_bucket
          and add it into the temporary vector
        - add it to the temporary vector, remove it from v_member
        - scan v_member again and repeat until v_member is empty
        a final padding member might be needed to make the cluster big enough
        for when cluster arrays are used (dimEG-related stuff)

        addressOffset is offset relative to the enclosing element (the cluster)
        addressOffset is in bytes!
        regProp.size is in bits!

        might be good to have a few helper variables as part of member_t
        - size_in_bytes
        - n_instances

    */
    // NOTE: this won't work for field_t where the offset/size are in bits!
    vector<member_ptr_t> v_tmp;
    constexpr size_t very_big = 1024*1024*1024; // huh, should be big enough for everyone
    size_t cur_p0 = 0;
    size_t cur_size = 0;
    size_t tmpsz = 0;
    size_t rec_idx;
    const size_t mc_s = v_member.size();
    while (!v_member.empty())
    {
        // find the member with lowest offset
        size_t rec_p0 = very_big;
        {
            size_t i = 0;
            tmpsz = 0;
            //for (member_ptr_t &memb : v_member)
            //cout << "  " << v_member.size() << "members:\n";
            while (i < v_member.size())
            {
                member_t *ppp0 = v_member[i].get_member();
                if (ppp0 != nullptr)
                {
                    member_t& mbr = *ppp0;
                    size_t t0 = mbr.addr_offs;
                    if (t0 < rec_p0) { rec_p0 = t0; rec_idx = i; tmpsz = mbr.total_size; }
                }
                else
                {
                    cout << "* Error: bad member in cluster " << name << "\n";
                    return false;
                }
                //cout << ">\n";
                ++i;
            }
            {
                //member_ptr_t &memb = v_member.at(rec_idx);
                //cout << name << " first memb: " << memb.get_name() << " total_size=" << tmpsz << "\n";
            }

        }
        // add it into v_tmp, possibly with a padding member before it
        {
            //cout << "z: " << rec_p0 << " " << cur_p0 << "\n";
            if (rec_p0 > cur_p0)
            {
                // if the lowest member found does not start from the current offset
                // insert a padding member before it
                //v_tmp.emplace_back();
                //parse_register& rbf = v_tmp.back();
                {
                    reg_t reg;
                    if (!make_padding_reg(mb, reg, cur_p0, (rec_p0 - cur_p0))) { return false; }
                    //res = reg.parse(svde);
                    v_tmp.emplace_back(reg, mb.v_reg);
                    cur_p0 += reg.total_size;
                }

                //cout << "  pad: " << (rec_p0 - cur_p0) << "\n";
                //make_padding_member(rbf, p0, (rec_p0 - p0));
                //rbf.proc_register(mod, true);
                //rbf.proc_bithint();
                //p0 += rbf.reg_size;
            }
            else if (rec_p0 < cur_p0)
            {
                // if the lowest bitfield member found starts from a lower offset than the current one
                //cout << "* Error: register overlap " << name << ".\n";
                //v_register.at(rec_i).capt = "OVERLAP! " + v_register.at(rec_i).capt;
                //return false;
            }
        }
        // add the lowest found member
        {
            //v_tmp.push_back(v_register.at(rec_i));
            //v_register.erase(v_register.begin()+rec_i);
            v_tmp.push_back(v_member.at(rec_idx));
            v_member.erase(v_member.begin()+rec_idx);
            //cur_size = v_tmp.back().reg_size;
            //assert(cur_size != 0);
            //cur_offs += cur_size;
            cur_size = tmpsz;
            cur_p0 += cur_size;
        }
    }
    // final padding member?
    {
        if (cur_p0 < single_sz) // known target size? dimIncrement? TODO!
        {
            // if the last member did not reach the end, append a padding member
            //v_tmp.emplace_back();
            //parse_register& rbf = v_tmp.back();
            /*
            rbf.name = "";
            rbf.capt = "padding";
            rbf.reg_offs = cur_offs;
            rbf.reg_size = (reg_size - cur_offs);
            rbf.proc_bithint();
            */
            reg_t reg;
            if (!make_padding_reg(mb, reg, cur_p0, (single_sz - cur_p0))) { return false; }
            v_tmp.emplace_back(reg, mb.v_reg);
            cur_p0 += reg.total_size;
        }
    }
    //cout << ".\n";
    assert(v_member.empty());
    v_member.swap(v_tmp);
    if (single_sz == 0) { single_sz = cur_p0; }
    //total_size = cur_p0; // this is probably wrong
    assert(single_sz == cur_p0);

    total_size = ni*single_sz;
    if (_is_array) { if (!adj_array_name(*this)) { return false; } }
    //cout << "Cluster " << name << " total_size=" << total_size << ", members before: " << mc_s << " / after: " << v_member.size() << "\n";
    {
        // make addr_offs and other fumbers look nice?
        // padding members' addr_offs was synthesized, so its fumber style is gonna be decimal or whatever
        // try to make it like one of the other legit registers:
        fumber64_t tmp_addr_offs; tmp_addr_offs.set("0");
        fumber64_t tmp_regp_size; tmp_regp_size.set("0");
        for (member_ptr_t &memb : v_member)
        {
            member_t *ppp0 = memb.get_member();
            if (ppp0 != nullptr)
            {
                member_t& mbr = *ppp0;
                if (mbr.is_padding == false)
                {
                    tmp_addr_offs.grow_style(mbr.addr_offs);
                    tmp_regp_size.grow_style(mbr.regProp.size);
                }
                else
                {
                    tmp_addr_offs.set_if_larger(mbr.addr_offs);
                    tmp_regp_size.set_if_larger(mbr.regProp.size);
                }
            }
        }
        tmp_addr_offs.recalc_style();
        tmp_regp_size.recalc_style();
        // now apply it to all?
        for (member_ptr_t &memb : v_member)
        {
            member_t *ppp0 = memb.get_member();
            if (ppp0 != nullptr)
            {
                member_t& mbr = *ppp0;
                mbr.addr_offs.copy_style(tmp_addr_offs);
                mbr.regProp.size.copy_style(tmp_regp_size);
            }
        }

    }
    {
        // this section plays the role of cluster_t::calc()

        // the above sorting+padding code allows overlapping members,
        // and doesn't calculate single_size and total_size correctly
        // which happens legitimately in particular when altReg/altCluster is used
        // and, actually, i think both altReg and altCluster are the same thing
        // so perhaps, member_t needs to have a common mechanism to mark this

        // TODO: scan for overlapping, scan for altReg/AltCluster
        // okay, so all members are mgroup 0, that means no group
        // scan the children, if there's altCluster or altReg - find the original one
        // check some things like, whether they are on the same offset and have the same size
        // then mark them as a common mgroup (above 0)
        // NOTE: it seems when the original register is an array "REG[%s]", altReg is also "REG[%s]"
        // what if a register calls for altReg of a cluster? or vice-versa..
        size_t rec_p0 = very_big;
        cur_p0 = 0;
        size_t cur_p1 = 0;
        size_t mgi = 1; // the current group
        for (member_ptr_t &memb : v_member)
        {
            switch (memb.mtype)
            {
                case MT_CLUSTER:
                {
                    cluster_t& clu = *memb.get_cluster();
                    if (rec_p0 > clu.addr_offs) { rec_p0 = clu.addr_offs; }
                    if (cur_p0 < clu.addr_offs) { cur_p0 = clu.addr_offs; }
                    const size_t tmp_p1 = cur_p0+clu.total_size;
                    if (cur_p1 < tmp_p1) { cur_p1 = tmp_p1; }
                    if (!clu.altCluster.empty())
                    {
                        if (clu.altCluster == clu.raw_name)
                        {
                            cout << "Error: cluster wants to be an alternative to itself.\n";
                            return false;
                        }
                        // find a member with that name, make sure it's a cluster
                        bool foundit = false;
                        for (member_ptr_t &membX : v_member)
                        {
                            member_t& mbr = *membX.get_member();
                            if (clu.altCluster == mbr.raw_name)
                            {
                                if (membX.mtype == MT_CLUSTER)
                                {
                                    foundit = true;
                                    cluster_t& cluX = *(membX.get_cluster());
                                    if (cluX.addr_offs != clu.addr_offs)
                                    {
                                        cout << "Error: altCluster and reference cluster are not at the same offset.\n";
                                        return false;
                                    }
                                    if (cluX.total_size != clu.total_size)
                                    {
                                        cout << "Error: altCluster and reference cluster are not the same total size.";
                                        cout << " (" << cluX.total_size << " vs " << clu.total_size << ")\n";
                                        //return false;
                                    }
                                    if (mbr.mgroup == 0)
                                    {
                                        mbr.mgroup = mgi;
                                        ++mgi;
                                    }
                                    clu.mgroup = mbr.mgroup;
                                    break;
                                }
                                else
                                {
                                    cout << "Error: altCluster refers to a member which is not a cluster\n";
                                    return false;
                                }
                            }
                        }
                        if (!foundit)
                        {
                            cout << "Error: couldn't find reference for altCluster(" << clu.altCluster << ")\n";
                            return false;
                        }
                    }
                } break;

                case MT_REG:
                {
                    reg_t &reg = *memb.get_reg();
                    if (rec_p0 > reg.addr_offs) { rec_p0 = reg.addr_offs; }
                    if (cur_p0 < reg.addr_offs) { cur_p0 = reg.addr_offs; }
                    const size_t tmp_p1 = cur_p0+reg.total_size;
                    if (cur_p1 < tmp_p1) { cur_p1 = tmp_p1; }
                    if (!reg.altReg.empty())
                    {
                        if (reg.altReg == reg.raw_name)
                        {
                            cout << "Error: register wants to be an alternative to itself.\n";
                            return false;
                        }
                        // find a member with that name, make sure it's a register
                        bool foundit = false;
                        for (member_ptr_t &membX : v_member)
                        {
                            member_t& mbr = *membX.get_member();
                            if (reg.altReg == mbr.raw_name)
                            {
                                if (membX.mtype == MT_REG)
                                {
                                    foundit = true;
                                    reg_t& regX = *(membX.get_reg());
                                    if (regX.addr_offs != reg.addr_offs)
                                    {
                                        cout << "Error: altRegister and reference register are not at the same offset.\n";
                                        return false;
                                    }
                                    if (regX.total_size != reg.total_size)
                                    {
                                        cout << "Error: altRegister and reference register are not the same total size.";
                                        cout << " (" << regX.total_size << " vs " << reg.total_size << ")\n";
                                        return false;
                                    }
                                    if (mbr.mgroup == 0)
                                    {
                                        mbr.mgroup = mgi;
                                        ++mgi;
                                    }
                                    reg.mgroup = mbr.mgroup;
                                    break;
                                }
                                else
                                {
                                    cout << "Error: altRegister refers to a member which is not a register\n";
                                    return false;
                                }
                            }
                        }
                        if (!foundit)
                        {
                            cout << "Error: couldn't find reference for altRegister(" << reg.altReg << ")\n";
                            return false;
                        }
                    }
                } break;
            }
        }
        if (rec_p0 != 0)
        {
            cout << "Error: cluster(" << name << ") lowest member is not at offset 0.\n";
            return false;
        }
        single_sz = cur_p1;
        total_size = ni*single_sz;
    }
    return true;
}

bool cluster_t::finalize(member_bucket_t &mb)
{
    // final scan to process things like names..?
    // deal with the name
    {
        #if 1
        if (decl_typename.empty())
        {
            skip_first_delimiter sfd("_");
            for (const svd_inh_path& ipath : mb.v_ipath)
            {
                string varname = ipath.varname;
                str_rem_surrounding_chars(varname, "_");
                decl_typename += sfd() + varname;
            }
            if (headerStructName.empty()) { headerStructName = decl_typename; }
            decl_typename += "_clu";
        }
        if (headerStructName.empty()) { headerStructName = name; }
        else { decl_typename = headerStructName; } // hm
        // unfortunately, registers don't have a headerStructName thing
        // when they have to be defined as union/struct type, their name
        // is synthesized with svd_inh_ipath
        /*
            the peripheral naming issue:
            STM32L496 - Serial Audio interface (SAI)
                61520  |   |   |  <e> "name" ::: "SAI1"
                61521  |   |   |  <e> "description" ::: "Serial audio interface"
                61522  |   |   |  <e> "groupName" ::: "SAI"
                61523  |   |   |  <e> "baseAddress" ::: "0x40015400"

                62353  |   |  <e> "peripheral" ::: ""
                       |   |  [a] "derivedFrom" = "SAI1"
                62354  |   |   |  <e> "name" ::: "SAI2"
                62355  |   |   |  <e> "baseAddress" ::: "0x40015800"
                62356  |   |   |  <e> "interrupt" ::: ""
                62361  |   |   |  <e> "interrupt" ::: ""

            Note: docs say <description> is mandatory for derived periphs
            clearly what needs to be done here is:
                struct SAI { regs; };
                extern SAI SAI1;
                extern SAI SAI2;


            STM32L496 - Timers
                  340  |   |  <e> "peripheral" ::: ""
                  341  |   |   |  <e> "name" ::: "TIM2"
                  342  |   |   |  <e> "description" ::: "General-purpose-timers"
                  343  |   |   |  <e> "groupName" ::: "TIM"
                  344  |   |   |  <e> "baseAddress" ::: "0x40000000"

                  418  |   |  <e> "peripheral" ::: ""
                       |   |  [a] "derivedFrom" = "TIM2"
                  419  |   |   |  <e> "name" ::: "TIM3"
                  420  |   |   |  <e> "baseAddress" ::: "0x40000400"
                  421  |   |   |  <e> "interrupt" ::: ""

                  446  |   |  <e> "peripheral" ::: ""
                  447  |   |   |  <e> "name" ::: "TIM15"
                  448  |   |   |  <e> "description" ::: "General purpose timers"
                  449  |   |   |  <e> "groupName" ::: "TIM"
                  450  |   |   |  <e> "baseAddress" ::: "0x40014000"

            TIM2 and TIM15 are complete and unique definitions
            TIM3 is derived from TIM2
            yet the GroupName is TIM everywhere, so that doesn't work

            ########################################################
            GD32F4xx - CAN
                 1103  |   |  <e> "peripheral" ::: ""
                 1104  |   |   |  <e> "name" ::: "CAN0"
                 1105  |   |   |  <e> "description" ::: "Controller area network"
                 1106  |   |   |  <e> "groupName" ::: "CAN"
                 1107  |   |   |  <e> "baseAddress" ::: "0x40006400"

                14513  |   |  <e> "peripheral" ::: ""
                       |   |  [a] "derivedFrom" = "CAN0"
                14514  |   |   |  <e> "name" ::: "CAN1"
                14515  |   |   |  <e> "baseAddress" ::: "0x40006800"
                14516  |   |   |  <e> "interrupt" ::: ""
                14520  |   |   |  <e> "interrupt" ::: ""
                14524  |   |   |  <e> "interrupt" ::: ""
                14528  |   |   |  <e> "interrupt" ::: ""

            Note: no <description> again
            same situation as with STM32L496

            ########################################################
            SAME54N20A - ADC
                  676  |   |  <e> "peripheral" ::: ""
                  677  |   |   |  <e> "name" ::: "ADC0"
                  678  |   |   |  <e> "version" ::: "U25001.0.0"
                  679  |   |   |  <e> "description" ::: "Analog Digital Converter"
                  680  |   |   |  <e> "groupName" ::: "ADC"
                  681  |   |   |  <e> "prependToName" ::: "ADC_"
                  682  |   |   |  <e> "baseAddress" ::: "0x43001C00"

                 1846  |   |  <e> "peripheral" ::: ""
                       |   |  [a] "derivedFrom" = "ADC0"
                 1847  |   |   |  <e> "name" ::: "ADC1"
                 1848  |   |   |  <e> "baseAddress" ::: "0x43002000"
                 1849  |   |   |  <e> "interrupt" ::: ""
                 1853  |   |   |  <e> "interrupt" ::: ""
            Note: no <description>
            same situation as above, but prependToName is provided

            ########################################################
            Atmel.ATSAMD21J18A - PORT
                 7847  |   |  <e> "peripheral" ::: ""
                 7848  |   |   |  <e> "name" ::: "PORT"
                 7849  |   |   |  <e> "version" ::: "1.0.0"
                 7850  |   |   |  <e> "description" ::: "Port Module"
                 7851  |   |   |  <e> "groupName" ::: "PORT"
                 7852  |   |   |  <e> "prependToName" ::: "PORT_"
                 7853  |   |   |  <e> "baseAddress" ::: "0x41004400"

                 8246  |   |  <e> "peripheral" ::: ""
                       |   |  [a] "derivedFrom" = "PORT"
                 8247  |   |   |  <e> "name" ::: "PORT_IOBUS"
                 8248  |   |   |  <e> "description" ::: "Port Module (IOBUS)"
                 8249  |   |   |  <e> "groupName" ::: "PORT_IOBUS"
                 8250  |   |   |  <e> "prependToName" ::: "PORT_IOBUS_"
                 8251  |   |   |  <e> "baseAddress" ::: "0x60000000"

            this is so weird on many levels
            - most (if not all) members of PORT struct are dim 2 (arrays of 2) with
              dimIncrement = 0x80, and their names are like "DIM%s" .. not DIM[%s]
            - the derived peripheral has a suspiciously different name, and groupName
            - samd21a\include\component\port.h defines:
                struct PortGroup    { all; the; regs; here; no; arrays; *** };
                struct Port         { PortGroup Group[2]; };
                // *** okay, just PMUX[16] and PINCFG[32] are arrays
            - samd21a\include\samd21j18a.h defines:
                #define PORT              ((Port     *)0x41004400UL) // (PORT) APB Base Address
                #define PORT_IOBUS        ((Port     *)0x60000000UL) // (PORT) IOBUS Base Address
              so it ends up there are 4 legit instances of PortGroup
              two for "PORT" and two for "PORT_IOBUS"
            Note: groupName on PORT_IOBUS has to be ignored here

            ########################################################
            Microchip.ATSAMD21J18A - PORT
                 7847  |   |  <e> "peripheral" ::: ""
                 7848  |   |   |  <e> "name" ::: "PORT"
                 7849  |   |   |  <e> "version" ::: "1.0.0"
                 7850  |   |   |  <e> "description" ::: "Port Module"
                 7851  |   |   |  <e> "groupName" ::: "PORT"
                 7852  |   |   |  <e> "prependToName" ::: "PORT_"
                 7853  |   |   |  <e> "baseAddress" ::: "0x41004400"

                 8246  |   |  <e> "peripheral" ::: ""
                 |   |        [a] "derivedFrom" = "PORT"
                 8247  |   |   |  <e> "name" ::: "PORT_IOBUS"
                 8248  |   |   |  <e> "description" ::: "Port Module (IOBUS)"
                 8249  |   |   |  <e> "groupName" ::: "PORT_IOBUS"
                 8250  |   |   |  <e> "prependToName" ::: "PORT_IOBUS_"
                 8251  |   |   |  <e> "baseAddress" ::: "0x60000000"
            looks identical to the Atmel one above, and the version is the same
        */
        #else
        decl_typename = headerStructName;
        if (decl_typename.empty()) { decl_typename = name; }
        if (headerStructName.empty())
        {
            skip_first_delimiter sfd("_");
            for (const svd_inh_path& ipath : mb.v_ipath)
            {
                string varname = ipath.varname;
                str_rem_surrounding_chars(varname, "_");
                headerStructName += sfd() + varname;
            }
            headerStructName += "_clu";
        }
        #endif // 1
    }
    for (member_ptr_t &memb : v_member)
    {
        if (memb.mtype == MT_CLUSTER)
        {
            cluster_t& clu = *memb.get_cluster();
            scoped_ipath s_ipath(mb.v_ipath);
            svd_inh_path& ipath = s_ipath.get();
            {
                ipath.name = clu.name;
                ipath.varname = clu.name; //clu.altCluster; // not sure, testing
                if (ipath.varname.empty()) { ipath.varname = clu.name; }
            }
            clu.finalize(mb);
        }
    }
    // process the registers now
    for (member_ptr_t &memb : v_member)
    {
        if (memb.mtype == MT_REG)
        {
            //memb.get_reg()->process(); <- why was this commented-out?!
            reg_t &reg = *memb.get_reg();
            scoped_ipath s_ipath(mb.v_ipath);
            svd_inh_path& ipath = s_ipath.get();
            {
                ipath.name = reg.name;
                ipath.varname = reg.name; //reg.altReg; // not sure, testing
                if (ipath.varname.empty()) { ipath.varname = reg.name; }
            }
            reg.finalize(mb); // hm?
        }
    }
    // re-scan everything, yet again, to deal with the access
    {
        if (!_acc.parse_from_str(regProp.access))
        {
            cout << "Error: parsing cluster access type.\n";
            return false;
        }
        svd_access_t tmp;
        tmp._access = SVD_ACCESS_NONE;
        for (member_ptr_t &memb : v_member)
        {
            switch (memb.mtype)
            {
                case MT_CLUSTER:
                {
                    cluster_t& clu = *memb.get_cluster();
                    tmp._access |= clu._acc._access;
                } break;
                case MT_REG:
                {
                    reg_t& reg = *memb.get_reg();
                    tmp._access |= reg._acc._access;
                } break;
            }
        }
        tmp.proc();
        if (tmp._access != _acc._access)
        {
            cout << "Warning: cluster access differs from its members' access.\n";
        }
        _acc.proc();
    }
    return true;
}
bool reg_t::finalize(member_bucket_t &mb)
{
    if (!_acc.parse_from_str(regProp.access))
    {
        cout << "Error: parsing register access type.\n";
        return false;
    }
    if (v_field.size() > 0) // should this be (!is_simple) instead? TODO
    {
        svd_access_t tmp;
        tmp._access = SVD_ACCESS_NONE;
        for (field_t &fld : v_field)
        {
            if (!fld.finalize(mb))
            {
                cout << "Error: finalizing reg(" << name << ").field(" << fld.name << ")\n";
                return false;
            }
            tmp._access |= fld._acc._access;
        }
        tmp.proc();
        if (tmp._access != _acc._access)
        {
            cout << "Warning: register access differs from its fields' access.\n";
        }
    }
    _acc.proc();

    if (is_simple == false)
    {
        {
            skip_first_delimiter sfd("_");
            for (const svd_inh_path& ipath : mb.v_ipath)
            {
                string varname = ipath.varname;
                str_rem_surrounding_chars(varname, "_");
                decl_typename += sfd() + varname;
            }
            decl_typename += "_reg";
        }
    }
    return true;
}
bool field_t::finalize(member_bucket_t &mb)
{
    if (!_acc.parse_from_str(access))
    {
        cout << "Error: parsing field access type.\n";
        return false;
    }
    _acc.proc();
    return true;
}

bool make_padding_field(field_t& fld, const size_t offs, const size_t size)
{
    fld.name = "";
    fld.desc = "padding";
    fld.bitOffset.set("0");
    fld.bitOffset = offs;
    fld.bitWidth.set("0");
    fld.bitWidth = size;
    //cout << "* Adding padding field: " << fld.name << " size=" << size << "\n";
    return true;
}
bool reg_t::process(member_bucket_t &mb)
{
    //cout << "process() reg(" << name << ")\n";
    is_simple = false;
    if (dataType.empty())
    {
        switch (regProp.size)
        {
            case 8:     dataType = "uint8_t"; break;
            case 16:    dataType = "uint16_t"; break;
            case 32:    dataType = "uint32_t"; break;
            case 64:    dataType = "uint64_t"; break;
            default:
            {
                logger("// Error: unknown register dataType/size: ", regProp.size.str(), "\n");
                return false;
            }
        };
    }
    // i think if the access is empty, the default is read-write
    if (regProp.access.empty())
    {
        // maybe instead of mentioning this as a comment in the generated .hpp
        // use a type classifier or whatever it's called
        // like _IO volatile uint32_t REGA;
        // where _IO may be nothing but it's visible
        regProp.access = "read-write";
    }
    // a register does not have to have fields
    // if it does - it can be made a union with bitfields
    if (v_field.size() == 1)
    {
        // only one field? can we still simplify this register?
        // not sure if this IF {} should be here.. or elsewhere.. hm..
        const field_t& fld = v_field.at(0);
        bool okay = false;
        bool cp_name = false;
        bool cp_desc = false;
        do
        {
            // if both things are empty - okay
            // if fld.thing is empty and reg.thing isn't - okay
            // if fld.thing is not empty but reg.thing is - okay, copy it to reg.thing
            // if fld.thing != reg.thing - not okay
            if (fld.bitWidth.val != regProp.size.val) { break; }
            if (fld.name != name)
            {
                if (!name.empty()) { break; }
                else { cp_name = true; } // register with no name? this shouldn't happen
            }
            if (fld.desc != desc)
            {
                if (!desc.empty()) { break; }
                else { cp_desc = true; }
            }
            // anything else? TODO .. maybe the regProp stuff

            okay = true;
        } while (0);

        // fix this
        if (okay)
        {
            logger("// NOTE: simplifying reg(", name, ") containing only 1 field(", fld.name, ").\n");
            cout << "* Simplifying reg(" << name << ") field(" << fld.name << ")\n";
            if (cp_name) { name = fld.name; }
            if (cp_desc) { desc = fld.desc; }
            is_simple = true;
        }
    }
    if (v_field.empty()) { is_simple = true; }
    if (is_simple == true)
    {
        // simple register
        decl_typename = dataType;
    }
    else
    {
        // there are fields.. the register is potentially a union/struct type
        //fumber64_t x; x.set("0"); x = v_field.size();
        //decl_typename = "sumfin" + x.str() + "_t";

        // scan the fields and sort them, fill any gaps, check for conflicts?
        // this is gonna have to appear as a type name like PERIPH3_REGA_t
        // in the PERIPH3 structure, and there should be a  PERIPH3_REGA_t
        // union+struct/bitfields declaration before that
        // decl_typename should be something like PERIPH3_REGA_t
        // Note that this register could be deep into nested clusters, so the
        // decl_typename would probably need to be more like
        // PERIPH_CLUSTER1_CLUSTER2_CLUSER3_REGA_t or so
        for (field_t &fld : v_field)
        {
            scoped_ipath s_ipath(mb.v_ipath);
            svd_inh_path& ipath = s_ipath.get();
            {
                ipath.name = fld.name;
                ipath.varname = fld.derivedFrom; // not sure, testing
                if (ipath.varname.empty()) { ipath.varname = fld.name; }
            }
            #if 0
            {
                skip_first_delimiter sfd("::");
                for (const svd_inh_path& ipath : mb.v_ipath) // TESTING
                {
                    fld.test += sfd() + ipath.name;
                }
            }
            #endif // 0
            fld.is_bitfield = true; // hm
            if (fld.process(mb) == false) { return false; }
            if (fld.access.empty()) { fld.access = regProp.access; }
        }
        #warning "TODO"
        {
            // padding and sorting
            size_t cur_p0 = 0;
            size_t cur_sz = 0;
            size_t rec_i = 0;
            vector<field_t> v_tmp;
            while (v_field.size())
            {
                size_t rec_p0 = 1024; // should be big enough for everyone
                {
                    // find the field with the lowest bitOffset
                    size_t i = 0;
                    while (i < v_field.size())
                    {
                        const field_t& fld = v_field.at(i);
                        if (fld.bitOffset.val < rec_p0) { rec_p0 = fld.bitOffset.val; rec_i = i; }
                        ++i;
                    }
                }
                //cout << "    bf:" << cur_p0 << ".." << rec_p0 << " ";
                {
                    if (rec_p0 > cur_p0)
                    {
                        // if the lowest field found does not start from the current offset
                        // insert a padding field before it
                        v_tmp.emplace_back();
                        field_t& fld = v_tmp.back();
                        make_padding_field(fld, cur_p0, (rec_p0 - cur_p0));
                        fld.process(mb);
                        if (fld.access.empty()) { fld.access = regProp.access; }
                        cur_p0 += fld.bitWidth.val;
                        //cout << "*" << fld.bitWidth.val << ", ";
                    }
                    else if (rec_p0 < cur_p0)
                    {
                        // if the lowest field found starts from a lower offset than the current one
                        field_t& fld = v_field.at(rec_i);
                        logger("// Error: field overlap, reg(", name, "), field(", fld.name, ")\n");
                        fld.desc = "OVERLAP! " + fld.desc;
                        // return false?
                    }
                    // else { we found the exact thing, move on }
                }
                // add the lowest found member
                v_tmp.push_back(v_field.at(rec_i));
                v_field.erase(v_field.begin()+rec_i);
                cur_sz = v_tmp.back().bitWidth.val;
                cur_p0 += cur_sz;
                //cout << ">>" << cur_sz << "\n";
                if (cur_sz == 0)
                {
                    cout << "hm.. what?\n";
                }
            }
            if (cur_p0 < regProp.size.val)
            {
                // if the last field did not reach the end, append a padding field
                v_tmp.emplace_back();
                field_t& fld = v_tmp.back();
                make_padding_field(fld, cur_p0, (regProp.size.val - cur_p0));
                fld.process(mb);
                if (fld.access.empty()) { fld.access = regProp.access; }
            }
            v_field.swap(v_tmp);
            {
                size_t _total_sz = 0;
                for (const field_t& fld : v_field)
                {
                    if (fld.bitWidth.val == 0) { cout << "* dafuq *"; }
                    _total_sz += fld.bitWidth.val;
                }
                if (_total_sz != regProp.size.val)
                {
                    cout << "WARNING: bitfield size mismatch, reg(" << decl_typename << ")! "
                    << _total_sz << " != " << regProp.size.val << "\n";
                }
            }
        }
        // SORT the fields?
        // add padding bits (dummy field_t) in any gaps?
        // check overal size matches expected size?
    }
    if (!calc()) { return false; }

    if (_is_array) { if (!adj_array_name(*this)) { return false; } }
    //cout << "reg.proc(" << name << ") " << decl_typename << "\n";

    return true;
}
bool field_t::process(member_bucket_t &mb)
{
    // the fields
    // maybe deal with the access here?
    {
        // bit hint
        const size_t bo = bitOffset.val;
        const size_t bw = bitWidth.val;
        size_t ba = (bo+bw)-1;
        fumber_t fba; fba.set("0"); fba.val = ba;
        if (bw > 1)
        {
            bit_hint = bitOffset.str();
            str_rightalign(bit_hint, ' ', 2);
            bit_hint += "..";
        }
        else { bit_hint = "    "; }
        string tmp = fba.str();
        str_rightalign(tmp, ' ', 2);
        bit_hint += tmp;
        bit_hint = "bit: " + bit_hint;
    }
    enums.process(mb); // since when did we stop checking the return value? TODO
    return true;
}

bool enums_t::process(member_bucket_t &mb)
{
    z_cg_mw_name = 0;
    // if the enum labels begin with digits - that's not good
    size_t bad = 0;
    for (enumV_t& enm : v_enum)
    {
        size_t nlen = enm.name.length();
        if (nlen >= 1)
        {
            const char c = enm.name.at(0);
            if ((c >= '0') && (c <= '9'))
            {
                ++bad;
                enm.name = "_" + enm.name;
                nlen += 1;
            }
        }
        else
        {
            return false;
        }
        if (nlen > z_cg_mw_name) { z_cg_mw_name = nlen; }
    }
    if (bad != 0)
    {
        logger("// Warning: enumValue.name begins with a digit.\n");
        cout << "WARNING: renaming enumValue names because some contain leading digit.\n";
    }
    return true;
}

// #########################################################

// #########################################################

// #########################################################

// #########################################################

bool SVD_PARSER::gen_interrupts(std::ostream& os)
{
    vector<string> col1, col2;
    {
        for (interrupt_t& irp : m_bucket.v_int)
        {
            col1.emplace_back(irp.name);
            col2.emplace_back("= " + irp.value.str());
        }
        str_tabify_col(col1, ' ', 4);
        str_rightalign_col(col2, ' ', 4);
    }

    // perhaps a more-generic namespace alias can be made afterwards by the user
    // namespace DEVICENAME_IRQn = my_irqn; my_irqn::UART1_RXC
    string tmp_name = "namespace " + dvc.name + "_IRQn";
    cg_scope cgsn(os, cgi, tmp_name, "{", "}; // " + tmp_name);
    {
        cg_scope cgse(os, cgi, "enum"); // IRQn_Type
        skip_last_delimiter comma(",", m_bucket.v_int.size());
        size_t cidx = 0;
        for (interrupt_t& irp : m_bucket.v_int)
        {
            string s = col1[cidx] + col2[cidx] + comma();
            str_tabify(s, ' ', 4);
            os  << cgi()
                //<< irp.name << " = " << irp.value.str()
                << s
                //<< comma()
                << "//!< " << irp.desc << "\n";
            ++cidx;
        }
    }
    return true;
}

bool SVD_PARSER::gen(std::ostream& os)
{
    const string com1 = "//! ";
    os << com1 << "Generated by..: as_svd_parse" << ' ' << this_version_str << "\n";
    os << com1 << "Generated from: " << src_fn << "\n";
    os << com1 << "Device........: " << dvc.name << "\n";
    os << com1 << "Description...: " << dvc.desc << "\n";
    os << com1 << "Series........: " << dvc.series << "\n";
    os << com1 << "Vendor........: " << dvc.vendor << "\n";
    os << com1 << "VendorID......: " << dvc.vendorID << "\n";
    os << com1 << "Version.......: " << dvc.version << "\n";
    os  << "#ifndef " << dvc.name << "_INCLUDED\n"
        << "#define " << dvc.name << "_INCLUDED\n";

    if (!dvc.licenseText.empty())
    {
        cg_scoped_indent cgsi(cgi);
        os << "/***\n";
        {
            std::stringstream ss;
            ss << dvc.licenseText;
            cg_indent_ss(cgi, os, ss, "*   ");
        }
        os << "***/\n";
    }
    os << "\n";
    {
        os << com1 << "CPU Core...........: " << svd_cpu_name[dvc.cpu._cpu_id] << " (" << svd_cpu_name2[dvc.cpu._cpu_id] << ")\n";
        os << com1 << "Revision...........: " << dvc.cpu.revision << "\n";
        os << com1 << "Endiannes..........: " << dvc.cpu.endian << "\n";
        os << com1 << "Interrupts.........: " << dvc.cpu.deviceNumInterrupts.str_safe() << "\n";
        os << com1 << "MPU................: " << dvc.cpu.mpuPresent.str() << "\n";
        os << com1 << "FPU................: " << dvc.cpu.fpuPresent.str() << "\n";
        os << com1 << "FPU-DoublePrec.....: " << dvc.cpu.fpuDP.str_safe() << "\n";
        os << com1 << "SIMD/DSP...........: " << dvc.cpu.dspPresent.str_safe() << "\n";
        os << com1 << "Instruction Cache..: " << dvc.cpu.icachePresent.str_safe() << "\n";
        os << com1 << "Data Cache.........: " << dvc.cpu.dcachePresent.str_safe() << "\n";
        os << com1 << "Instruction-TCM....: " << dvc.cpu.itcmPresent.str_safe() << "\n";
        os << com1 << "Data-TCM...........: " << dvc.cpu.dtcmPresent.str_safe() << "\n";
        os << com1 << "VendorSystickConfig: " << dvc.cpu.vendorSystickConfig.str_safe() << "\n";

        os << "#define " << "__" << svd_cpu_name[dvc.cpu._cpu_id] << "_REV " << dvc.cpu.revision << "\n";
        os << "#define " << "__MPU_PRESENT " << dvc.cpu.mpuPresent.str() << "\n";
        os << "#define " << "__FPU_PRESENT " << dvc.cpu.fpuPresent.str() << "\n";
        os << "#define " << "__NVIC_PRIO_BITS " << dvc.cpu.nvicPrioBits.str() << "\n";
    }
    os << "\n";

    gen_interrupts(os);

    for (periph_t& per : v_periph)
    {
        if (!per.gen(os)) { return false; }
        os << std::endl;
    }

    os  << "#endif // " << dvc.name << "_INCLUDED\n";

    {
        cg_scope cgsc(os, cgi, "", "/*", "*/");
        for (reg_t &reg : m_bucket.v_reg)
        {
            os  << "R* " << reg.dataType << " " << reg.decl_name0
                << (!reg.decl_name1.empty() ? "***" : "") << reg.decl_name1 << " "
                << reg.raw_name
                << "\n";
        }
        for (cluster_t &clu : m_bucket.v_cluster)
        {
            os  << "C* " << clu.decl_typename << " " << clu.decl_name0
                << (!clu.decl_name1.empty() ? "***" : "") << clu.decl_name1 << " "
                << clu.raw_name
                << "\n";
        }
        for (periph_t& per : v_periph)
        {
            os  << "P* " << per.headerStructName << " " << per.name;
            if (!per.derivedFrom.empty()) { os << " (" << per.derivedFrom << ")"; }
            os  << "\n";
        }
    }

    return true;
}

const string indent(size_t level)
{
    if (level == 0) { return ""; }
    string s;
    while (level--)
    { s += "    "; }
    return s;
}

bool enums_t::gen_enum(std::ostream& os, const string varname, const string datatype) const
{
    //os << name << " - " << headerEnumName << " - " << usage
    //   << " - " << derivedFrom << "\n";
    string tmp_name = "struct " + varname + "_" + name
                    + " //!< scoped enum";
    cg_scope cgs1(os, cgi, tmp_name);
    {
        tmp_name = "enum : " + datatype;
        cg_scope cgs2(os, cgi, tmp_name);

        skip_last_delimiter comma(",", v_enum.size());
        for (const enumV_t &enm : v_enum)
        {
            string lhs = enm.name;
            string rhs = "= " + enm.value.str() + comma();
            str_tabify(lhs, ' ', 4, z_cg_mw_name);
            str_tabify(rhs, ' ', 8, 4);
            os  << cgi()
                //<< name << "_"
                << lhs << rhs
                << "//!< "
                << (enm.is_default ? "[default] " : "")
                << enm.desc << "\n";
        }
    }
    return true;
}

bool reg_t::gen_reg(std::ostream& os, const string varname) const
{
    if (is_simple == false)
    {
        string reg_final_typename = varname + name + "_reg";
        {
            // generate union-struct type register declaration
            string tmp_name = "union " + reg_final_typename + " //!< " + desc;
            cg_scope cgs1(os, cgi, tmp_name);

            os  << "// decl_typename: " << decl_typename << "\n";
            string decl_dtype = "_reg_t";

            {
                os  << cgi()
                    << "using " << decl_dtype << " = " << dataType << ";\n";
                os  << cgi()
                    << decl_dtype << " _reg;\n";
                //os  << cgi()
                    //<< "struct : " << dataType
                //tmp_name = "struct";
                {
                    cg_scope cgs2(os, cgi, "struct");
                    for (const field_t &fld : v_field)
                    {
                        string lhs = decl_dtype; // removed volatile from here
                        str_tabify(lhs, ' ', 4, 16);
                        lhs += fld.name; if (fld.name.empty()) { lhs += "        "; }
                        str_tabify(lhs, ' ', 8, 24);
                        lhs += ":";
                        string rhs = fld.bitWidth.str();
                        str_rightalign(rhs, ' ', 2);
                        lhs += rhs;
                        //str_tabify(lhs, ' ', 4);
                        os  << cgi()
                            //<< fld.name
                            << lhs
                            << "; //!< "
                            << "["
                            << fld.bit_hint << " "
                            << fld._acc._acc_hint
                            << "]"
                            << " "
                            << fld.desc // << " isDefault: " << enm.isDefault
                            << "\n";
                    }
                }

                //os  << cgi() << "// resetValue: " << regProp.resetValue.str() << "\n";
                //os  << cgi() << "// resetMask.: " << regProp.resetMask.str() << "\n";
                //os  << cgi() << "// protection: " << regProp.protection << "\n";
                {
                    // print maybe some useful constants?
                    std::stringstream ss;
                    bool gotany = false;
                    {
                        cg_scope cgs3(ss, cgi, "enum");
                        vector<string> vtmp;
                        if (regProp.resetValue.good())
                        { gotany = true; vtmp.emplace_back("_resetValue = " + regProp.resetValue.str()); }
                        if (regProp.resetMask.good())
                        { gotany = true; vtmp.emplace_back("_resetMask  = " + regProp.resetMask.str()); }

                        skip_last_delimiter comma(",", vtmp.size());
                        for (const string& stmp : vtmp)
                        {
                            ss << cgi() << stmp << comma() << "\n";
                        }
                    }
                    if (gotany)
                    {
                        os << ss.str();
                    }
                }
            }
        }
        // NOTE: decl_typename and varname+name+"_reg" seem to match, hm...
        // except when the register is an array, then:
        // decl_typename is like REG_NAME[%s]
        os  << cgi()
            << "static_assert(sizeof(" << reg_final_typename << ") == sizeof(" << dataType << "));\n\n";
    }
    return true;
}

bool periph_t::gen(std::ostream& os)
{
    //cout << "gen() periph(" << name << ")\n";
    const string com1 = "//! ";
    string s, b;
    b = com1; str_tabify(b, '-', wtbf);
    os << b << "\n";
    s = com1 + name; // + " "; str_tabify(s, '#', wtbf);
    os << s << "\n";
    s = com1 + desc;
    os << s << "\n";
    s = com1 + "version=\"" + version + "\"";
    os << s << "\n";
    s = com1; if (is_clone) { s += "Cloned from: " + derivedFrom; } else { s += "Unique definition"; }
    os << s << "\n";
    os << b << "\n" << std::endl;

    size_t lvli = 0;

    // maybe put a pre-declaration gen() call here which will do nothing for the cloned periphs?
    if (is_clone == false)
    {
        // periph_t has everything interesting stuffed into "regs", which is a cluster_t
        // so that's the main cluster
        // clusters can have altCluster, which typically means grouping into a union
        // all of them have to be on the same level!
        /*
            cluster SERCOM_I2C_M
            cluster SERCOM_I2C_S (altCluster=SERCOM_I2C_M)
            cluster SERCOM_USART (altCluster=SERCOM_I2C_M)

            union
            {
                SERCOM_I2C_M i2c_m;
                SERCOM_I2C_S i2c_s;
                SERCOM_USART usart;
            };
            assuming they all start on the same offset.. hm..
        */
        os << "// Peripheral's member types, enums, constants (if any) definitions:\n";
        {
            for (member_ptr_t &memb : regs.v_member)
            {
                switch (memb.mtype)
                {
                    case MT_REG:
                    {
                        const reg_t *ppp1 = memb.get_reg();
                        if (ppp1 != nullptr)
                        {
                            const reg_t &reg = *ppp1;
                            // nuffin for now
                            #if 1
                            string varname = prependToName + reg.name;
                            string decl_base = "constexpr " + reg.dataType;
                            str_tabify(decl_base, ' ', 4, 0);
                            for (const field_t &fld : reg.v_field)
                            {
                                #if 0
                                string decl_base2 = decl_base + " " + varname + "_" + fld.name;
                                string fld_desc;
                                if (!fld.desc.empty()) { fld_desc = " //!< " + fld.desc; }

                                {
                                    string decl_base3 = decl_base2 + "_Pos";
                                    str_tabify(decl_base3, ' ', 8, 0);
                                    os  << indent(lvli)
                                        << decl_base3
                                        << "= " << fld.bitOffset.str()
                                        << ";" << fld_desc
                                        << "\n";
                                }
                                {
                                    string decl_base3 = decl_base2 + "_Msk";
                                    str_tabify(decl_base3, ' ', 8, 0);
                                    os  << indent(lvli)
                                        << decl_base3
                                        << "= " << fld.bitWidth.str()
                                        << "; FIXME " << fld_desc
                                        << "\n";
                                }
                                #endif // 0
                                if (fld.has_enums())
                                {
                                    fld.enums.gen_enum(os, varname, reg.dataType);
                                }
                            }
                            string reg_name = prependToName;
                            if (reg_name.empty()) { reg_name = name + "_"; }
                            reg.gen_reg(os, reg_name);
                            #else
                            os  << indent(lvli)
                                << "enum " << prependToName << reg.name << " : " << reg.dataType
                                << " //!< " << reg.desc
                                << "\n"
                                << "{\n";
                            ++lvli;
                            for (const field_t &fld : reg.v_field)
                            {
                                //string varname = reg.name <<  fld.name
                                os  << indent(lvli)
                                    << fld.name << "_Pos" << " = " << fld.bitOffset.str()
                                    << "; //!< " << fld.desc
                                    << "\n";

                                    //<< "constexpr " << "T" << ' ' << fld.name << "; //!< "
                                    //<<
                            }
                            --lvli;
                            os  << indent(lvli)
                                << "}\n";
                            #endif // 1
                        }
                    } break;
                    case MT_CLUSTER:
                    {
                        cluster_t *ppp2 = memb.get_cluster();
                        if (ppp2 != nullptr)
                        {
                            cluster_t &clu = *ppp2;
                            clu.gen(os);
                        }
                    } break;
                    case MT_NONE:
                        cerr << "Error: cluster(" << name << ").gen(): bad member.\n";
                        return false;
                }
            }
        }
        //os << "// TODO: structure definition here!\n";
        // ---------- main periph struct begin
        os  << cgi() << com1
            << "Peripheral definition for " << headerStructName
            << ", " << (1+cloned) << " instances.\n";
        if (!nspace.empty())
        {
            os << cgi() << "namespace " << nspace << "\n{\n";
        }
        {
            string tmp_name = "struct " + headerStructName;
            cg_scope cgs1(os, cgi, tmp_name);
            //s = "struct " + headerStructName + "\n{";
            //os << s << "\n";
            // ---------- main periph struct contents:

            os << cgi() << "// prependToName......: " << prependToName << "\n";
            os << cgi() << "// appendToName.......: " << appendToName << "\n";
            os << cgi() << "// alternatePeripheral: " << alternatePeripheral << "\n";
            os << cgi() << "// groupName..........: " << groupName << "\n";
            os << cgi() << "// headerStructName...: " << headerStructName << "\n";
            os << cgi() << "// regs.headerStructN.: " << regs.headerStructName << "\n";
            {
                // debug/test stuff..
                for (const svd_addrBlock_t &ab : v_addrBlock)
                {
                    os  << cgi() << "// "
                        << "addrBlock: offset(" << ab.offset.str()
                        << ") size(" << ab.size.str()
                        << ") protection(" << ab.protection
                        << ") usage(" << ab.usage
                        << ")\n";
                }
            }
            size_t z_nclu = 0; // number of first-child clusters
            size_t z_nreg = 0; // number of first-child registers

            // volatile const  uint32_t             STATUS; //!<
            // volatile        CTRLA_reg             CTRLA; //!<
            // volatile        INTENSET_reg       INTENSET; //!<
            // volatile        uint8_t           _rsvd4[3]; //!<

            // -cv_qual------  -decl_typename-- -----name-

            vector<string> col1, col2, col3;
            for (member_ptr_t &memb : regs.v_member)
            {

                switch (memb.mtype)
                {
                    case MT_REG:
                    {
                        ++z_nreg;
                        const reg_t *ppp1 = memb.get_reg();
                        if (ppp1 != nullptr)
                        {
                            const reg_t &reg = *ppp1;
                            s = "";
                            col1.emplace_back(reg._acc._cv_qual);
                            col2.emplace_back(reg.decl_typename);
                            if (reg._is_array)
                            {
                                s = reg.dimEG.dim.str();
                                s = reg.decl_name0 + s + reg.decl_name1;
                                //s += "REG.ARY";
                            }
                            else
                            {
                                s = reg.name;
                            }
                            col3.emplace_back(s);

                            #if 0
                            for (const field_t &fld : reg.v_field) // TESTING
                            {
                                os << "    // " << fld.test << "\n";
                            }
                            {
                                // debug/test stuff..
                                bool doit = false;
                                if (!reg.v_field.empty())       { doit = true; }
                                if (!reg.altReg.empty())        { doit = true; }
                                if (!reg.derivedFrom.empty())   { doit = true; }
                                if (!reg.altGroup.empty())      { doit = true; }
                                if (doit)
                                {
                                    os  << "        // DN:" << reg.displayName << " N: " << reg.name << " AR: " << reg.altReg
                                        << " AG: " << reg.altGroup << " dF: " << reg.derivedFrom << " fieldCount: " << reg.v_field.size()
                                        << "\n";
                                    for (const field_t &fld : reg.v_field)
                                    {
                                        #if 1
                                        //os  << "    constexpr " << "T" << ' ' << fld.name << "; //!< "
                                        //    <<
                                        #else
                                        os  << "        // -- field: " << fld.bitOffset.str() << "(" << fld.bitWidth.str() << ") "
                                            << fld.name << " -- A:" << fld.access
                                            << " enumCount: " << fld.v_enum.size() << " mWV: " << fld.modifiedWriteValues
                                            << " --- D: " << fld.desc
                                            << "\n";
                                        #endif // 1
                                    }
                                }
                            }
                            #endif // 0
                        }
                    } break;
                    case MT_CLUSTER:
                    {
                        ++z_nclu;
                        const cluster_t *ppp2 = memb.get_cluster();
                        if (ppp2 != nullptr)
                        {
                            const cluster_t &clu = *ppp2;
                            s = "";
                            col1.emplace_back(clu._acc._cv_qual);
                            col2.emplace_back(clu.decl_typename);
                            if (clu._is_array)
                            {
                                s = clu.dimEG.dim.str();
                                s = clu.decl_name0 + s + clu.decl_name1;
                                //s += "CLU.ARY";
                            }
                            else
                            {
                                s = clu.name;
                            }
                            col3.emplace_back(s);
                        }
                    } break;
                    case MT_NONE:
                        cerr << "Error: periph(" << name << ").gen(): bad member.\n";
                        return false;
                }
            }
            assert(regs.v_member.size() == col1.size());
            str_tabify_col(col1);
            str_tabify_col(col2);
            str_rightalign_col(col3);
            size_t midx = 0;
            size_t cur_mgroup = 0;
            cg_scope_m cgsm(os, cgi, "union");
            for (member_ptr_t &memb : regs.v_member)
            {
                #if 1
                const member_t* ppp0 = memb.get_member();
                if (ppp0 != nullptr)
                {
                    const member_t &mbr = *ppp0;
                    if (cur_mgroup != mbr.mgroup)
                    {
                        if (cur_mgroup != 0) { cgsm.close(); }
                        if (mbr.mgroup != 0) { cgsm.open("union // group " + std::to_string(mbr.mgroup)); }
                        cur_mgroup = mbr.mgroup;
                    }
                    os  << cgi()
                        << col1[midx] << col2[midx] << col3[midx]
                        << "; //!< "
                        << mbr.mgroup << " " << mbr.total_size << " "
                        << "[+" << mbr.addr_offs.str() << ", sz=" << mbr.regProp.size.str()
                        << ", " << mbr._acc._acc_hint
                        << "] "
                        << mbr.desc
                        << "\n";
                }
                #else
                switch (memb.mtype)
                {
                    case MT_REG:
                    {
                        const reg_t *ppp1 = memb.get_reg();
                        if (ppp1 != nullptr)
                        {
                            const reg_t &reg = *ppp1;
                            os  << cgi()
                                << col1[midx] << col2[midx] << col3[midx]
                                << "; //!< "
                                << "[+" << reg.addr_offs.str() << ", sz=" << reg.regProp.size.str()
                                << ", " << reg._acc._acc_hint
                                << "] "
                                << reg.desc
                                << "\n";
                        }
                    } break;
                    case MT_CLUSTER:
                    {
                        const cluster_t *ppp2 = memb.get_cluster();
                        if (ppp2 != nullptr)
                        {
                            const cluster_t &clu = *ppp2;
                            os  << cgi()
                                << col1[midx] << col2[midx] << col3[midx]
                                << "; //!< "
                                << "[+" << clu.addr_offs.str() << ", sz=" << clu.regProp.size.str()
                                << ", " << clu._acc._acc_hint
                                << "] "
                                << clu.desc
                                << "\n";
                        }
                    } break;
                }
                #endif // 0
                ++midx;
            }
            if (cur_mgroup != 0) { cgsm.close(); }
            os << "// clusters(" << z_nclu << "), registers(" << z_nreg << ")\n";

            // ---------- main periph struct closing
        }
        os  << cgi()
            << "static_assert(sizeof(" << headerStructName << ") == " << regs.total_size << ");\n";
        if (!nspace.empty())
        {
            os  << cgi()
                << "} // namespace " << nspace << "\n";
        }
    }

    // instance declaration:
    // TODO: maybe prepend the device/svd name in front of this (and similar) macro(s)?
    cg_pp_if ppdb(os, cgi,"#ifndef", "_NO_INSTANCE_DEF_" + name);
    s = nspace;
    if (!nspace.empty()) { s += "::"; }
    os << com1 << "Peripheral (" << s << headerStructName << ") " << name << " instance at " << baseAddr.str() << "\n";
    //os << s << headerStructName << " " << name << "; //!< @ " << baseAddr.str() << "\n";
    {
        // peripheral instance declaration using extern + asm() scheme
        // if the peripheral instance is in a namespace, the syntax seems to be:
        // namespace THE_NAMESPACE { extern PERIPH_t NAME; }
        // no idea how that works with the asm() scheme, TODO
        string tmp_name = s + headerStructName; // typename (with any namespace)
        {
            string lhs = R"(asm(".set    )";
            string rhs = name;
            constexpr size_t tw = 8; // tabify/align width
            str_tabify(lhs, ' ', 4, tw);
            str_rightalign(rhs, ' ', tw);
            lhs += rhs;
            lhs += ", ";
            str_tabify(lhs, ' ', 4);
            rhs = baseAddr.str();
            str_rightalign(rhs, ' ', 12);
            rhs += "\");";
            {
                // add a helpful comment with the address in alternative form (decimal/hex)
                fumber64_t addr2; addr2.set(baseAddr.str());
                if (addr2.type == fumber64_t::T_DECIMAL)  { addr2.type = fumber64_t::T_HEX; addr2.num_chars = 8; }
                else if (addr2.type == fumber64_t::T_HEX) { addr2.type = fumber64_t::T_DECIMAL; }
                rhs += " // " + addr2.str();
            }
            os << cgi() << lhs << rhs << "\n";

            lhs = R"(asm(".global )";
            rhs = name;
            str_tabify(lhs, ' ', 4, tw);
            str_rightalign(rhs, ' ', tw);
            rhs += "\");";
            os << cgi() << lhs << rhs << "\n";
        }
        {
            string lhs = "extern " + tmp_name;
            str_tabify(lhs, ' ', 4, 8);
            string rhs = name;
            str_rightalign(rhs, ' ', 8);
            os << cgi() << lhs << rhs << "; //!< " << desc << "\n";
        }
    }

    //os << "\n";
    return true;
}
bool cluster_t::gen(std::ostream& os)
{
    const string com1 = "//! ";
    string s, b;
    if (is_clone == false)
    {
        os << "// TODO: cluster's member types (if any) definitions here!\n";
        //os << "// TODO: structure definition here!\n";
        // ---------- main cluster struct begin
        os  << com1 << "Cluster definition for " << headerStructName << ", " << (1+cloned) << " instances.\n";
        //s = "struct " + headerStructName + "\n{";
        size_t z_nclu = 0;
        size_t z_nreg = 0;
        {
            string tmp_name = "struct " + decl_typename;
            cg_scope cgs1(os, cgi, tmp_name);
            //if (!nspace.empty()) { s = "namespace " + nspace + "\n{\n" + s; }
            //os << s << "\n";
            // ---------- main cluster struct contents:
            os << cgi() << "// altCluster.........: " << altCluster << "\n";
            os << cgi() << "// name...............: " << name << "\n";
            os << cgi() << "// headerStructName...: " << headerStructName << "\n";
            os << cgi() << "// decl_typename......: " << decl_typename << "\n";

            vector<string> col1, col2, col3;
            for (member_ptr_t &memb : v_member)
            {
                switch (memb.mtype)
                {
                    case MT_REG:
                    {
                        ++z_nreg;
                        const reg_t *ppp1 = memb.get_reg();
                        if (ppp1 != nullptr)
                        {
                            const reg_t &reg = *ppp1;
                            s = "";
                            col1.emplace_back(reg._acc._cv_qual);
                            col2.emplace_back(reg.decl_typename);
                            if (reg._is_array)
                            {
                                s = reg.dimEG.dim.str();
                                s = reg.decl_name0 + s + reg.decl_name1;
                                //s += "REG.ARY";
                            }
                            else
                            {
                                s = reg.name;
                            }
                            col3.emplace_back(s);
                        }
                    } break;
                    case MT_CLUSTER:
                    {
                        ++z_nclu;
                        const cluster_t *ppp2 = memb.get_cluster();
                        if (ppp2 != nullptr)
                        {
                            const cluster_t &clu = *ppp2;
                            s = "";
                            col1.emplace_back(clu._acc._cv_qual);
                            col2.emplace_back(clu.decl_typename);
                            if (clu._is_array)
                            {
                                s = clu.dimEG.dim.str();
                                s = clu.decl_name0 + s + clu.decl_name1;
                                //s += "CLU.ARY";
                            }
                            else
                            {
                                s = clu.name;
                            }
                            col3.emplace_back(s);
                        }
                    } break;
                    case MT_NONE:
                        cerr << "Error: cluster(" << name << ").gen(): bad member.\n";
                        return false;
                }
            }
            assert(v_member.size() == col1.size());
            str_tabify_col(col1);
            str_tabify_col(col2);
            str_rightalign_col(col3);
            size_t midx = 0;
            for (member_ptr_t &memb : v_member)
            {
                const member_t* ppp0 = memb.get_member();
                if (ppp0 != nullptr)
                {
                    const member_t &mbr = *ppp0;
                    os  << cgi()
                        << col1[midx] << col2[midx] << col3[midx]
                        << "; //!< "
                        << mbr.mgroup << " " << mbr.total_size << " "
                        << "[+" << mbr.addr_offs.str() << ", sz=" << mbr.regProp.size.str()
                        << ", " << mbr._acc._acc_hint
                        << "] "
                        << mbr.desc
                        << "\n";
                }
                ++midx;
            }
            os << "// clusters(" << z_nclu << "), registers(" << z_nreg << ")\n";

            // ---------- main cluster struct closing
        }
        os  << cgi()
            << "static_assert(sizeof(" << decl_typename << ") == " << total_size << ");\n";

        // ---------- main cluster struct closing
        //if (!nspace.empty()) { s += "\n}"; }
        //os << s << "\n";
    }
    os << "// total_size(" << total_size << ") single_sz(" << single_sz << ")\n";
    return true;
}
