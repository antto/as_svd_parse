#ifndef SVD_STUFF_HPP_INCLUDED
#define SVD_STUFF_HPP_INCLUDED

#include "util.hpp"
#include <tinyxml2.h>
#include <vector>

/*
    SVD:

    ----------------------------------------------------
    device
        vendor
        name
        cpu
        peripherals
            peripheral (may have attribute derivedFrom)
                name
                description
                baseAddress
                addressBlock
                    offset
                    size
                    usage
                    protection
                interrupt
                registers
                    cluster (may have attribute derivedFrom)
                        dim
                        addressOffset
                    register

    ----------------------------------------------------
    "derivedFrom" afaiu means you clone that thing, and then stuff in this this override
    the original values
    in the case of clusers, a derived cluster may use a cluster from the current periph, or
    even from another periph, thus the name of the derived cluster has to be examined
    for example "periphB.cluster3" ..
    for a derived cluster, the name/description/addressOffset will probably be overriden

    "dim" stuff is for arrays, and the names of these things might have "[%s]"

    if "headerStructName" is not specified, the name of the structure is made from the name
    of the periph

    there must be at least one "addressBlock" per non-derived periph
    a field (or many) can only be inside a fields element
    a fields (only one) can only be inside a register element
    a register (or many) can be inside registers or cluster

    registers
        reg
        reg
            fields
                field
                field
    registers
        reg
        cluster
        reg
        cluster
            reg
                fields
                    field
                    field
    registers
        reg
        cluster
            reg
            reg
            cluster
                reg
                reg
                    fields
                        field
                        field

    ----------------------------------------------------
    so:
    periph_t { regz_t }
    regz_t { member_t[] }
    member_t<reg_t>
    member_t<cluster_t>
    cluster_t { member_t[] }

    where member_t is base class for two kinds of members - reg_t or cluster_t
    hm.. cluster_t would appear similar to regz_t .. what's the dif then?

    ------
    or
*/

using fumber64_t = fumber_t<uint64_t>;
using fumberbool_t = fumber_t<bool>;
using std::cout, std::cerr;
using std::string, std::to_string, std::size_t, std::vector;
using namespace tinyxml2;
static z_logger logger;

// https://wandbox.org/permlink/6dNC6XkfKqV6QWCy

enum PARSER_RETCODE
{
    RC_SUCCESS,
    RC_ERROR_NULLPTR,
    RC_ERROR_PERIPHERALS,
    RC_ERROR_ADDR_BLOCK,
    RC_ERROR_EMPTY_PERIPHERALS,
    RC_ERROR_SVD_ELEMENT,
    RC_SKIP, // for debugging
    RC_NO_CHILDREN,
    RC_NO_NEXT
};
struct reg_t;
struct cluster_t;
struct member_bucket_t;
struct svd_element_t;

/// ############################################################################
/// ############################################################################
/// ############################################################################

/// SVD dimElementGroup
/**
 * Elements from this group can be part of:
 * @li \b peripheral
 * @li \b register
 * @li \b cluster
 * @li \b field
 * The values are used as defaults, and each next level can override them.
 */
struct svd_dimEG_t
{
    // these elements can be part of:
    // - peripheral
    // - register
    // - cluster
    // - field
    // Q: if something uses dim stuff, its name is like NAME[%s], what if there's also
    // something else derived from it? does it say derivedFrom=NAME[%s] ?? or just NAME ??
    fumber64_t dim;             //!< Defines the number of elements in an array or list.
    fumber64_t dimIncrement;    //!< Specify the address increment between two neighboring array or list members in the address map.
    /** \brief Specify the strings that substitue the placeholder %s within \b name and \b displayName. */
    /**
        By default, \b dimIndex is a value starting with 0.
        @note Do not define \b dimIndex when using the placeholder [%s] in \b name or \b displayName.
        */
    string dimIndex;
    string dimName; //!< Specify the name of the C-type structure. If not defined, then the entry in the \b name element is used.
    //string dimArrayIndex; //!< Grouping element to create enumerations in the header file.
};

/// SVD registerPropertiesGroup
/**
 * Elements from this group can be part of:
 * @li \b device
 * @li \b peripheral
 * @li \b register
 * @li \b cluster
 * The values are used as defaults, and each next level can override them.
 */
struct svd_regPropG_t
{
    // elements from this group can be part of:
    // - device
    // - peripheral
    // - register
    // - cluster
    // the values are used as defaults, and each next level can override them
    // Element values defined on a lower level overwrite element values defined on a more general level.
    // For example, <register>.<size> overwrites <peripheral>.<size>.
    // Elements not defined on a more general level, must be defined at <register> level at the latest.
    fumber64_t size; //!< Define the default bit-width of any device register (implicit inheritance). The value can be redefined on any lower level using the <size> element there.
    //! Define access rights. Access rights can be redefined at any lower level. Use one of the following predefined values:
    /*!
    \param \b read-only: Read access is permitted. Write operations have an undefined result.
    \param \b write-only: Read operations have an undefined result. Write access is permitted.
    \param \b read-write: Read and write accesses are permitted. Writes affect the state of the register and reads return the register value.
    \param \b writeOnce: Read operations have an undefined results. Only the first write after reset has an effect.
    \param \b read-writeOnce: Read access is always permitted. Only the first write access after a reset will have an effect on the content. Other write operations have an undefined result.
    */
    string access;
    /** \brief Specify the security privilege to access an address region. */
    /**
     *  This information is relevant for the programmer as well as the debugger when no universal access permissions have been granted. If no specific information is provided, an address region is accessible in any mode. The following values can be used to protect accesses by the programmer or debugger:
     * @li \b "s" - secure permission required for access
     * @li \b "n" - non-secure or secure permission required for access
     * @li \b "p" - privileged permission required for access
     */
    string protection;
    fumber64_t resetValue; //!< Define the default value for all registers at RESET. The value can be redefined on any lower level using the \b resetValue element there. The actual reset value is calculated from the \b resetValue and the \b resetMask. The mask is used to specify bits with an undefined reset value.
    fumber64_t resetMask; //!< Identify register bits that have a defined reset value. These bit positions are set to 1. Bit positions with an undefined reset value are set to 0.
};

enum svd_access_e : uint8_t
{
    SVD_ACCESS_NONE= 0,
    SVD_ACCESS_RW  = 0b00000001, //!< read-write, normal
    SVD_ACCESS_RO  = 0b00000010, //!< read-only
    SVD_ACCESS_WO  = 0b00000100, //!< write-only
    SVD_ACCESS_W1  = 0b00001000, //!< writeONCE, reading is UB
    SVD_ACCESS_RW1 = 0b00010000, //!< read-writeONCE
};

struct svd_access_t
{
    svd_access_t() : _access(SVD_ACCESS_NONE)
    {}
    // helper struct to contain the access things
    // access property have the following elements:
    // - periph
    // - cluster
    // - reg
    // - field
    uint8_t  _access;   //!< access flags (multiple might be combined)
    string _cv_qual;    //!< access specifier for like member declarations (const, etc..)
    string _acc_hint;   //!< access hint (for the description), like RW/RO/etc..

    bool parse_from_str(const string access) /// false on error
    {
        _access = SVD_ACCESS_NONE;
        _cv_qual.erase();
        if      (access.empty())                { _access = SVD_ACCESS_NONE; }
        else if (access == "read-write")        { _access = SVD_ACCESS_RW; }
        else if (access == "read-only")         { _access = SVD_ACCESS_RO; }
        else if (access == "write-only")        { _access = SVD_ACCESS_WO; }
        else if (access == "writeOnce")         { _access = SVD_ACCESS_W1; }
        else if (access == "read-writeOnce")    { _access = SVD_ACCESS_RW1; }
        else { return false; }

        return true;
    }
    void proc() /// generate the _acc_spec string
    {
        _cv_qual = "volatile";
        switch (_access)
        {
            default:
            case SVD_ACCESS_NONE:
                _acc_hint = "???";
                break;
            case SVD_ACCESS_RW:
                _cv_qual = "volatile";
                _acc_hint = "RW_";
                break;
            case SVD_ACCESS_RO:
                _cv_qual = "volatile const";
                _acc_hint = "R__";
                break;
            case SVD_ACCESS_WO:
                _cv_qual = "volatile";
                _acc_hint = "_W_";
                break;
            case SVD_ACCESS_W1:
                _cv_qual = "volatile";
                _acc_hint = "_W1";
                break;
            case SVD_ACCESS_RW1:
                _cv_qual = "volatile";
                _acc_hint = "RW1";
                break;
        }
    }
    /*
        RW  - R W - volatile
        RO  - R   - volatile const? (members of a struct with this need to be initialized, meh)
        WO  -   W - volatile
        W1  -   W - volatile
        RW1 - R W - volatile

        so it seems only if the whole thing is read-only and nothing else,
        can we even begin to consider putting const
    */
};

/// SVD addressBlock
/**
 *  Specify an address range uniquely mapped to this peripheral.
 *  A peripheral must have at least one address block.
 *  If a peripheral is derived form another peripheral, the \b addressBlock is not mandatory.
 */
struct svd_addrBlock_t
{
    // this is only found in peripheral
    fumber64_t offset; //!< Specifies the start address of an address block relative to the peripheral \b baseAddress.
    /** Specifies the number of addressUnitBits being covered by this address block.
        The end address of an address block results from the sum of baseAddress, offset, and (size - 1). */
    fumber64_t size;
    /**
     *  The following predefined values can be used:
     *  @li \b registers
     *  @li \b buffer
     *  @li \b reserved.
     */
    string usage;
    string protection; //!< Set the protection level for an address block.

    PARSER_RETCODE parse(const svd_element_t& root_svde);
};

enum MEMBER_TYPE
{
    MT_NONE,
    MT_REG,
    MT_CLUSTER
};

struct inh_member_t /// the inheritable part of member_t/reg_t/cluster_t
{
    string desc; /// description
    svd_regPropG_t regProp;
    svd_dimEG_t dimEG;
    // the following are not from the SVD:
    svd_access_t _acc;
    string decl_typename; //!< like uint32_t or name of union/struct
    string decl_name0; //!< like  "PIN_CTRL["
    string decl_name1; //!< empty or like    "]"
};
// ############ MEMBER_T ############
struct member_t : public inh_member_t
{
    member_t() : is_clone(false), _is_array(false), cloned(0), is_padding(false), mgroup(0)
    {}
    string name;
    string raw_name; //!< unlike name, any %s or [%s] are kept here
    //fumber64_t size;
    // both reg_t and cluster_t have this addressOffset property which isn't part of
    // anything common, thus i'm moving it to member_t (NOTE: NOT inh_member_t)
    fumber64_t      addr_offs; //!< Clusters: address relative to the baseAddress of the peripheral. Regs: address offset relative to the enclosing element.
    virtual MEMBER_TYPE get_mtype() { return MT_NONE; }
    // ------ the following stuff is not part of SVD
    size_t single_sz; //!< size in bytes (for one element, in case of arrays)
    size_t ni; //!< number of instances/elements (1 if it's not an array)
    size_t total_size; //!< size in bytes for the whole (in case of this being an array)
    // --------
    bool is_clone;
    bool _is_array; //!< true even if the size is 1 element, as long as declaration needs name[n] style
    size_t cloned; //!< for the original, fully-specified member, this counts the number of clones
    bool is_padding; //!< is this an artificially made padding register? (always false for clusters)
    size_t mgroup; //!< member group - 0 means "normal", anything above 0 is a group
    string derivedFrom; //!< the name of the reference member (derivedFrom)
};

struct member_ptr_t
{
    // this should be able to point to any object based on member_t
    // you should be able to make a vector<> of member_ptr_t
    // requires a member_bucket_t
    member_ptr_t() : mtype(MT_NONE), v(nullptr), idx(0) {}
    //member_ptr_t(const MEMBER_TYPE mt, member_bucket_t &mb);
    member_ptr_t(vector<reg_t>      &rv);
    member_ptr_t(vector<cluster_t>  &rv);
    template <typename T>
    member_ptr_t(T& r, vector<T>    &rv)
    {
        mtype = r.get_mtype();
        if (mtype == MT_NONE) { throw std::logic_error("* E: creating a member_ptr from something bad."); }
        idx = rv.size();
        rv.push_back(r);
        v = &rv;
    }

    MEMBER_TYPE mtype;
    void *v;
    size_t idx;


    reg_t* get_reg() /// nullptr if it's not a reg
    {
        if ((mtype == MT_REG) && (v != nullptr))
        {
            vector<reg_t> &vv = *reinterpret_cast<vector<reg_t>*>(v);
            return &vv.at(idx);
        }
        return nullptr;
    }
    cluster_t* get_cluster() /// nullptr if it's not a cluster
    {
        if ((mtype == MT_CLUSTER) && (v != nullptr))
        {
            vector<cluster_t> &vv = *reinterpret_cast<vector<cluster_t>*>(v);
            return &vv.at(idx);
        }
        cerr << "W: getting cluster.\n";
        return nullptr;
    }
    member_t* get_member(); /// nullptr if it's nor okay
    const member_t* get_member() const /// nullptr if it's nor okay
    {
        //return get_member(); // this seems to NOT call the non-const get_member, but calls itself recursively o_O
        return const_cast<member_ptr_t*>(this)->get_member();
    }
    string get_name() const;
    void derive(const member_ptr_t &from)
    {
        // hm...
        // wait.. cluster/reg vs member_ptr
        #warning "FIXME"
        /*
        mtype = from.get_mtype();
        if (mtype == MT_NONE) { throw std::logic_error("* E: deriving a member_ptr from something bad."); }

        idx = f.size();
        rv.push_back(r);
        v = &rv;
        */
    }
};

struct svd_inh_path //!< SVD inheritance path helper object
{
    // a vector/stack of these could be made.. perhaps
    string name;
    string varname;
};

using scoped_ipath = scoped_vector_element<svd_inh_path>;

struct enumV_t
{
    enumV_t() : is_default(false)
    {}
    PARSER_RETCODE parse(const svd_element_t& root_svde);
    string name; //!< String describing the semantics of the value. Can be displayed instead of the value.
    string desc; //!< Extended string describing the value.
    /**
        Defines the constant for the bit-field as decimal, hexadecimal (0x...) or binary (0b... or #...) number.
        In addition the binary format supports 'do not care' bits represented by x. */
    fumber64_t value;
    string isDefault; //!< Defines the name and description for all other values that are not listed explicitly.
    // ----------------
    bool is_default; //!< bool value from the isDefault string
};

struct enums_t
{
    // the parrent of this can only be a field
    enums_t() : is_clone(false), cloned(0)
    {}
    PARSER_RETCODE parse(const svd_element_t& root_svde);
    bool process(member_bucket_t &mb);
    //bool gen(std::ostream& os, const string varname, const string datatype) const;

    string name; //!< Identifier for the whole enumeration section.
    string headerEnumName; //!< Identifier for the enumeration section. Overwrites the hierarchical enumeration type in the device header file. User is responsible for uniqueness across description.
    string usage; //!< Possible values are read, write, or read-write. This allows specifying two different enumerated values depending whether it is to be used for a read or a write access. If not specified, the default value read-write is used.

    vector<enumV_t> v_enum; // rename this to v_value
    // ----------------
    bool is_clone;
    size_t cloned; //!< for the original, fully-specified enums_t, this counts the number of clones
    string derivedFrom; //!< Makes a copy from a previously defined enumeratedValues section. No modifications are allowed. An enumeratedValues entry is referenced by its name. If the name is not unique throughout the description, it needs to be further qualified by specifying the associated field, register, and peripheral as required.
    bool gen_enum(std::ostream& os, const string varname, const string datatype) const;
    size_t z_cg_mw_name; //!< max name length
};

struct inh_field_t
{
    string access; //!< Predefined strings set the access type. The element can be omitted if access rights get inherited from parent elements.
    svd_access_t _acc;
    svd_dimEG_t dimEG;

};
struct field_t : public inh_field_t
{
    field_t() : is_clone(false), is_padding(false), is_bitfield(false), cloned(0)
    {}
    PARSER_RETCODE parse(const svd_element_t& root_svde);
    bool process(member_bucket_t &mb);
    bool finalize(member_bucket_t &mb);
    string name; //!< Name string used to identify the field. Field names must be unique within a register.
    string desc; //!< String describing the details of the register.
    // there are 3 different ways to specify the field position and size:
    // - bitRangeLsbMsbStyle:
    fumber64_t bitOffset; //!< Value defining the position of the least significant bit of the field within the register.
    fumber64_t bitWidth; //!< Value defining the bit-width of the bitfield within the register.
    // - bitRangeOffsetWidthStyle:
    fumber64_t lsb; //!< Value defining the bit position of the least significant bit within the register.
    fumber64_t msb; //!< Value defining the bit position of the most significant bit within the register.
    // - bitRangePattern:
    string bitRange; //!< A string in the format: "[msb:lsb]"
    string modifiedWriteValues; //!< Describe the manipulation of data written to a field. If not specified, the value written to the field is the value stored in the field.

    //enumeratedValues // may have derivedFrom!
    //vector<enumV_t> v_enum;
    enums_t enums;
    // ----------------
    bool is_clone;
    bool is_padding;
    bool is_bitfield;
    size_t cloned; //!< for the original, fully-specified field, this counts the number of clones
    string derivedFrom; //!< the name of the reference field (derivedFrom)
    string bit_hint; //!< fancy bit offset/width visualisation
    inline bool has_enums() const { return !enums.v_enum.empty(); }
    //string test;
};
struct reg_t : public member_t
{
    // this would contain some actual data
    // any reason for this not having a constructor???
    PARSER_RETCODE parse(const svd_element_t& root_svde);
    bool process(member_bucket_t &mb);
    bool finalize(member_bucket_t &mb);

    MEMBER_TYPE get_mtype() { return MT_REG; }

    /**
      * When specified, then this string can be used by a graphical frontend to visualize the register.
      * Otherwise the \b name element is displayed. \b displayName may contain special characters and white spaces.
      * You can use the placeholder \b %s, which is replaced by the \b dimIndex substring. Use the placeholder \b [%s]
      * only at the end of the identifier. The placeholder \b [%s] cannot be used together with \b dimIndex.
      */
    string          displayName;
    /**
     *  It can be useful to assign a specific native C datatype to a register.
     *  This helps avoiding type casts. For example, if a 32 bit register shall act
     *  as a pointer to a 32 bit unsigned data item, then dataType can be set to "uint32_t *".
     */
    string          dataType;
    /**
     *  Specifies a group name associated with all alternate register that have the same name.
     *  At the same time, it indicates that there is a register definition allocating the same absolute
     *  address in the address space.
     */
    string          altGroup;
    /**
     *  This tag can reference a register that has been defined above to current location in the
     *  description and that describes the memory location already.
     *  This tells the SVDConv's address checker that the redefinition of this particular register is intentional.
     *  The register name needs to be unique within the scope of the current peripheral.
     *  A register description is defined either for a unique address location or could be a redefinition of an already described address.
     *  In the latter case, the register can be either marked alternateRegister and needs to have a unique name,
     *  or it can have the same register name but is assigned to a register subgroup through the
     *  tag alternateGroup (specified in version 1.0).
     */
    string          altReg;
    //fumber64_t      addr_offs; //!< Define the address offset relative to the enclosing element.
    vector<field_t> v_field;
    // ----------
    bool is_simple; //!< does it need a custom (union/bitfield) type declaration or a dumb "uint32_t" works?
    // ----------
    bool calc()
    {
        _is_array = false;
        total_size = 0;
        single_sz = (regProp.size.val / 8); // TODO: this shouldn't be hard-coded 8, should come from <device>
        if ((single_sz*8) != regProp.size.val)
        {
            logger("// Error: member size calculation conflict, size=", regProp.size.val, "bits\n");
            return false;
        }
        ni = 1;
        // TODO: if dim/dimIncrement are specified, they both have to be valid!
        // add proper checking for that!
        uint8_t z_array = 0;
        {
            const uint8_t d0 = dimEG.dim.good();
            const uint8_t d1 = dimEG.dimIncrement.good();
            z_array = (d0 ^ d1) | (d0 & d1);
        }
        if (z_array) //(dimEG.dim.good())
        {
            _is_array = true;
            single_sz = dimEG.dimIncrement.val;
            ni = dimEG.dim.val;
        }
        if ((ni == 0) || (single_sz == 0))
        {
            logger("// Error: member size calculation error, size=", single_sz, "*", ni, "\n");
            return false;
        }
        total_size = ni*single_sz;
        return true;
    }
    bool gen_reg(std::ostream& os, const string varname) const;
};
struct cluster_t : public member_t
{
    PARSER_RETCODE parse(const svd_element_t& root_svde, member_bucket_t &mb);
    bool process(member_bucket_t &mb);
    bool finalize(member_bucket_t &mb);

    MEMBER_TYPE get_mtype() { return MT_CLUSTER; }
    //string name; // member_t .. why wasn't this moved?
    //string desc; // member_t
    string headerStructName; //!< Specify the struct type name created in the device header file. If not specified, then the name of the cluster is used.
    string altCluster; //!< Specify the name of the original cluster if this cluster provides an alternative description.
    //fumber64_t              addr_offs; //!< Cluster address relative to the \b baseAddress of the peripheral.
    //svd_regPropG_t          regProp; // member_t has this
    //svd_dimEG_t             dimEG;   // member_t has this
    vector<member_ptr_t>    v_member;
    // ---------- these are not part of SVD
    //bool has_altMembers;
    //bool calc() { } //hm.. maybe clusters shall not have calc() since they are trickier
    bool gen(std::ostream& os);
};

struct interrupt_t
{
    // required:
    string              name; //!< The string represents the interrupt name.
    fumber_t<int32_t>   value; //!< Represents the enumeration index value associated to the interrupt.
    // optional:
    string              desc; //!< The string describes the interrupt.
};

struct member_bucket_t
{
    // this should store objects that are based on member_t
    vector<reg_t>       v_reg;
    vector<cluster_t>   v_cluster;

    vector<svd_inh_path> v_ipath;
    vector<interrupt_t>  v_int;
};

//! members of the peripheral which get inherited when derivedFrom is used
struct inh_periph_t
{
    string          desc;
    string          version; //!< The string specifies the version of this peripheral description.
    /** Define a name under which the System Viewer is showing this peripheral. */
    string          groupName;
    /** Define a string as prefix. All register names of this peripheral get this prefix. */
    string          prependToName;
    /** Define a string as suffix. All register names of this peripheral get this suffix. */
    string          appendToName;
    /** Specify the base name of C structures.
        The headerfile generator uses the name of a peripheral as the base name for the C structure type.
        If \b headerStructName element is specfied, then this string is used instead of the peripheral name;
        useful when multiple peripherals get derived and a generic type name should be used. */
    string          headerStructName;
    svd_dimEG_t     dimEG;
    //svd_regPropG_t  regPropG; // moving this to registers_t but it needs to be inherited!
    //svd_addrBlock_t addrBlock; //!< Note: this is one level deeper!!!
    vector<svd_addrBlock_t> v_addrBlock;
};
struct periph_t : public inh_periph_t
{
    periph_t() : is_clone(false), cloned(0)
    {}
    /**
        The string identifies the peripheral.
        Peripheral names are required to be unique for a device.
        The name needs to be an ANSI C identifier to generate the header file.
        You can use the placeholder \b [%s] to create arrays. */
    string          name;
    /**
        All address blocks in the memory space of a device are assigned to a unique peripheral by default.
        If multiple peripherals describe the same address blocks, then this needs to be specified explicitly.
        A peripheral redefining an address block needs to specify the name of the peripheral that is listed first in the description. */
    string alternatePeripheral; // wtf is this? a union {} of peripherals at an address maybe
    fumber64_t      baseAddr; //!< Lowest address reserved or used by the peripheral.
    // ----
    PARSER_RETCODE parse(const svd_element_t& root_svde, member_bucket_t &mb);
    PARSER_RETCODE parse_interrupt(const svd_element_t& root_svde, interrupt_t& irp);
    bool process(member_bucket_t &mb);
    bool finalize(member_bucket_t &mb);
    bool gen(std::ostream& os);
    cluster_t     regs; //!< The actual registers.
    vector<interrupt_t> v_int;
    string nspace; //!< namespace prepend string.. this is not part of SVD
    // ---------
    string get_name() const { return name; }
    bool is_clone;
    size_t cloned; //!< for the original, fully-specified periph, this counts the number of clones
    string derivedFrom; //!< the name of the reference peripheral (derivedFrom)
    void derive(const periph_t& from)
    {
        baseclass_copy<inh_periph_t>(*this, from);
        regs.regProp = from.regs.regProp;
    }
};

struct svd_cpu
{
    enum : uint32_t
    {
        CM0 = 0,    //!< Arm Cortex-M0
        CM0PLUS,    //!< Arm Cortex-M0+
        CM1,        //!< Arm Cortex-M1
        SC000,      //!< Arm Secure Core SC000
        CM23,       //!< Arm Cortex-M23
        CM3,        //!< Arm Cortex-M3
        CM33,       //!< Arm Cortex-M33
        CM35P,      //!< Arm Cortex-M35P
        CM55,       //!< Arm Cortex-M55
        SC300,      //!< Arm Secure Core SC300
        CM4,        //!< Arm Cortex-M4
        CM7,        //!< Arm Cortex-M7
        CA5,        //!< Arm Cortex-A5
        CA7,        //!< Arm Cortex-A7
        CA8,        //!< Arm Cortex-A8
        CA9,        //!< Arm Cortex-A9
        CA15,       //!< Arm Cortex-A15
        CA17,       //!< Arm Cortex-A17
        CA53,       //!< Arm Cortex-A53
        CA57,       //!< Arm Cortex-A57
        CA72,       //!< Arm Cortex-A72
        _N //!< number of cpu types, keep this last!
    };
};

static const char* const svd_cpu_name[svd_cpu::_N] =
{

    "CM0",
    "CM0PLUS",
    "CM1",
    "SC000",
    "CM23",
    "CM3",
    "CM33",
    "CM35P",
    "CM55",
    "SC300",
    "CM4",
    "CM7",
    "CA5",
    "CA7",
    "CA8",
    "CA9",
    "CA15",
    "CA17",
    "CA53",
    "CA57",
    "CA72"
    //other: other processor architectures
};
static const char* const svd_cpu_name2[svd_cpu::_N] =
{

    "Arm Cortex-M0",
    "Arm Cortex-M0+",
    "Arm Cortex-M1",
    "Arm Secure Core SC000",
    "Arm Cortex-M23",
    "Arm Cortex-M3",
    "Arm Cortex-M33",
    "Arm Cortex-M35P",
    "Arm Cortex-M55",
    "Arm Secure Core SC300",
    "Arm Cortex-M4",
    "Arm Cortex-M7",
    "Arm Cortex-A5",
    "Arm Cortex-A7",
    "Arm Cortex-A8",
    "Arm Cortex-A9",
    "Arm Cortex-A15",
    "Arm Cortex-A17",
    "Arm Cortex-A53",
    "Arm Cortex-A57",
    "Arm Cortex-A72"
    //other: other processor architectures
};

struct cpu_t
{
    cpu_t()
    : _cpu_id(svd_cpu::_N)
    {}
    // required:
    string name;
    string revision; //!< Define the HW revision of the processor. The version format is rNpM (N,M = [0 - 99]).
    /** Define the endianness of the processor being one of:
     *
     *  little: little endian memory (least significant byte gets allocated at the lowest address).
     *  big: byte invariant big endian data organization (most significant byte gets allocated at the lowest address).
     *  selectable: little and big endian are configurable for the device and become active after the next reset.
     *  other: the endianness is neither little nor big endian.
     */
    string endian;
    fumberbool_t mpuPresent; //!< Indicate whether the processor is equipped with a memory protection unit (MPU). This tag is either set to true or false, 1 or 0.
    fumberbool_t fpuPresent; //!< Indicate whether the processor is equipped with a hardware floating point unit (FPU). Cortex-M4, Cortex-M7, Cortex-M33 and Cortex-M35P are the only available Cortex-M processor with an optional FPU. This tag is either set to true or false, 1 or 0.
    fumber64_t nvicPrioBits; //!< Define the number of bits available in the Nested Vectored Interrupt Controller (NVIC) for configuring priority.
    fumberbool_t vendorSystickConfig; //!< Indicate whether the processor implements a vendor-specific System Tick Timer. If false, then the Arm-defined System Tick Timer is available. If true, then a vendor-specific System Tick Timer must be implemented. This tag is either set to true or false, 1 or 0.

    // optional:
    fumberbool_t fpuDP; //!< Indicate whether the processor is equipped with a double precision floating point unit. This element is valid only when <fpuPresent> is set to true. Currently, only Cortex-M7 processors can have a double precision floating point unit.
    fumberbool_t dspPresent; //!< Indicates whether the processor implements the optional SIMD DSP extensions (DSP). Cortex-M33 and Cortex-M35P are the only available Cortex-M processor with an optional DSP extension. For ARMv7M SIMD DSP extensions are a mandatory part of Cortex-M4 and Cortex-M7. This tag is either set to true or false, 1 or 0.. This element is mandatory for Cortex-M33, Cortex-M35P and future processors with optional SIMD DSP instruction set.
    fumberbool_t icachePresent; //!< Indicate whether the processor has an instruction cache. Note: only for Cortex-M7-based devices.
    fumberbool_t dcachePresent; //!< Indicate whether the processor has a data cache. Note: only for Cortex-M7-based devices.
    fumberbool_t itcmPresent; //!< Indicate whether the processor has an instruction tightly coupled memory. Note: only an option for Cortex-M7-based devices.
    fumberbool_t dtcmPresent; //!< Indicate whether the processor has a data tightly coupled memory. Note: only for Cortex-M7-based devices.
    fumberbool_t vtorPresent; //!< Indicate whether the Vector Table Offset Register (VTOR) is implemented in Cortex-M0+ based devices. This tag is either set to true or false, 1 or 0. If not specified, then VTOR is assumed to be present.
    fumber64_t deviceNumInterrupts; //!< Add 1 to the highest interrupt number and specify this number in here. You can start to enumerate interrupts from 0. Gaps might exist between interrupts. For example, you have defined interrupts with the numbers 1, 2, and 8. Add 9 :(8+1) into this field.
    fumber64_t sauNumRegions; //!< Indicate the amount of regions in the Security Attribution Unit (SAU). If the value is greater than zero, then the device has a SAU and the number indicates the maximum amount of available address regions.
    //sauRegionsConfig; //!< If the Secure Attribution Unit is preconfigured by HW or Firmware, then the settings are described here.

    PARSER_RETCODE parse(const svd_element_t& root_svde, member_bucket_t &mb);
    // ------
    uint32_t _cpu_id; //!< see svd_cpu::enum
};

struct svd_device_t
{
    // optional:
    string vendor;      //!< Specify the vendor of the device using the full name.
    string vendorID;    //!< Specify the vendor abbreviation without spaces or special characters. This information is used to define the directory.
    string series;      //!< Specify the name of the device series.
    /**
        The text will be copied into the header section of the generated device header file
        and shall contain the legal disclaimer. New lines can be inserted by using \n.
        This section is mandatory if the SVD file is used for generating the device header file. */
    string licenseText;
    cpu_t cpu;          //!< Describe the processor included in the device.
    /**
        Specify the file name (without extension) of the device-specific system include
        file (system_<device>.h;
        See CMSIS-Core description).
        The header file generator customizes the include statement referencing the CMSIS
        system file within the CMSIS device header file.
        By default, the filename is system_device-name.h.
        In cases where a device series shares a single system header file, the name of the
        series shall be used instead of the individual device name. */
    string headerSystemFilename;
    /**
        This string is prepended to all type definition names generated in the CMSIS-Core
        device header file. This is used if the vendor's software requires vendor-specific types
        in order to avoid name clashes with other definied types. */
    string headerDefinitionsPrefix;

    // required:
    string name;            //!< The string identifies the device or device series. Device names are required to be unique.
    string desc;            //!< Describe the main features of the device (for example CPU, clock frequency, peripheral overview).
    string version;         //!< Define the version of the SVD file. Silicon vendors maintain the description throughout the life-cycle of the device and ensure that all updated and released copies have a unique version string. Higher numbers indicate a more recent version.
    fumber64_t addrUnitBits;    //!< Define the number of data bits uniquely selected by each address. The value for Cortex-M-based devices is 8 (byte-addressable).
    fumber64_t width;           //!< Define the number of data bit-width of the maximum single data transfer supported by the bus infrastructure. This information is relevant for debuggers when accessing registers, because it might be required to issue multiple accesses for resources of a bigger size. The expected value for Cortex-M-based devices is 32.
};

/// ############################################################################

struct svd_attr_t
{
    string aname, aval;
};
/// helper object for parsing elements
struct svd_element_t
{
    svd_element_t() : valid(false), has_attr(false), pe(nullptr), level(0), path(".") {}
    string ename, etext;
    bool valid, has_attr;
    vector<svd_attr_t> v_attr;
    int doc_line;
    const XMLElement *pe;
    size_t level;
    string path;

    //! returns RC_SUCCESS or RC_NO_CHILDREN
    PARSER_RETCODE go_in_and_read(vector<svd_attr_t> *v_a = nullptr, const string& where = "")
    {
        if (pe == nullptr) { throw std::logic_error("go_in_and_read on a nullptr?"); return RC_ERROR_NULLPTR; }
        const XMLElement *e = pe->FirstChildElement();
        if (e != nullptr)
        {
            ++level;
            pe = e;
            const string old_ename = ename;
            if (read(pe, v_a))
            {
                path += '/';
                if (where.empty()) { path += old_ename; }
                else { path += where; }
                return RC_SUCCESS;
            }
            return RC_ERROR_SVD_ELEMENT;
        }
        return RC_NO_CHILDREN;
    }
    //! returns RC_SUCCESS or RC_NO_NEXT
    PARSER_RETCODE go_down_and_read(vector<svd_attr_t> *v_a = nullptr)
    {
        if (pe == nullptr) { throw std::logic_error("go_down_and_read on a nullptr?"); return RC_ERROR_NULLPTR; }
        const XMLElement *e = pe->NextSiblingElement();
        if (e != nullptr)
        {
            pe = e;
            if (read(pe, v_a)) { return RC_SUCCESS; }
            return RC_ERROR_SVD_ELEMENT;
        }
        return RC_NO_NEXT;
    }
    bool read(const XMLElement *e, vector<svd_attr_t> *v_a = nullptr)
    {
        valid = false;
        has_attr = false;
        doc_line = -1;
        pe = nullptr;
        v_attr.clear();
        if (e == nullptr) { return false; }
        pe = e;
        doc_line = e->GetLineNum();
        if (!get_element_name(e, ename)) { return false; }
        if (!get_element_text(e, etext))
        {
            // i think if there's no text, then there *must* be children elements
            //cout << "* Warning: requesting text from element which does not contain text:\n";
            //print_element(e);
            if (e->FirstChildElement() == nullptr)
            {
                // i thought this was not allowed in SVD but atsame54n20a.svd has
                // <description/>
                cout << "* Warning: element \"" << ename << "\" with no text and no children (line: " << e->GetLineNum() << ").\n";
                etext = "<empty>";
                //return false;
            }
        }
        #if 0
        if (str_compare(ename, (string)(e->Value())) == false)
        {
            cout << "* Warning: element \"" << ename << "\" has value \"" << e->Value() << "\" (line: " << e->GetLineNum() << ").\n";
        }
        #endif // 0
        // check for attributes... most elements don't have them
        const XMLAttribute *a = e->FirstAttribute();
        if (a != nullptr)
        {
            has_attr = true;
            // load the attributes
            while (a != nullptr)
            {
                svd_attr_t attr = { a->Name(), a->Value() };
                v_attr.push_back(attr);
                a = a->Next();
            }
            if (v_a != nullptr) { *v_a = v_attr; }
        }
        valid = true;
        return true;
    }

    template<typename Tstr, typename T>
    bool get(const Tstr what, T& target)
    {
        if (valid)
        {
            if (str_compare(what, ename))
            {
                if constexpr (std::is_same<T, std::string>::value)
                {
                    // some SVDs have newlines often
                    string tmp = etext;
                    if ((ename == "description") || (ename == "licenseText"))
                    {
                        if (str_rem_newlines2(tmp))
                        {
                            //cout << "* I: removing newlines and indentation from <description>.\n";
                        }
                        target = tmp;
                    }
                    else
                    {
                        if (str_rem_newlines(tmp))
                        {
                            cout << "* W: element " << ename << " had multi-line text.\n";
                            //cout << "* W: element " << ename << " had multi-line text: \"" << tmp << "\"\n";
                        }
                        target = tmp;
                    }
                    return true;
                }
                target = etext;
                return true;
            }
            return false;
        }
        throw std::logic_error("get(x,y) on an invalid svde.");
        return false;
    }
    template<typename Tstr>
    bool get(const Tstr what)
    {
        if (valid)
        {
            if (str_compare(what, ename)) { return true; }
            return false;
        }
        throw std::logic_error("get(x) on an invalid svde.");
        return false;
    }

    void print(std::ostream& os)
    {
        if (valid == false) { os << "svd_element_t<INVALID>\n"; return; }
        fumber64_t lnum; lnum = "0"; lnum.num_chars = 6;
        lnum = doc_line;
        string tab;
        #if 0
        if (level > 0) { tab = string(level*4, ' '); }
        #else
        if (level > 0)
        {
            //tab = string(level*4, ' ');
            size_t vi = level;
            if (vi >= 1)
            {
                //--vi;
                while (vi--) { tab += "  | "; }
            }
            //tab += "  | ";
        }
        #endif // 1
        os << lnum.str() << tab << " <e> \"" << ename << "\" ::: \"" << etext << "\"\n";
        for (const svd_attr_t& a : v_attr)
        {
            os << tab <<"       [a] \"" << a.aname << "\" = \"" << a.aval << "\"\n";
        }
    }
    void print_w_unhandled(std::ostream& os)
    {
        os << "Warning: unhandled element(level=" << level << "): " << ename << "\n";
    }
};

/// ############################################################################
extern const string this_version_str;
struct SVD_PARSER
{
    PARSER_RETCODE parse(const XMLElement *root);
    PARSER_RETCODE parsenprint(const XMLElement *root, const size_t start_level = 0);
    bool memb_check_sizes(vector<member_ptr_t> &mpv);
    bool process();
    bool gen(std::ostream& os);
    bool gen_interrupts(std::ostream& os);

    // ----------------
    svd_element_t       svde;
    string              header_fnb; //!< proposed filename for the header (no extension)
    string              src_fn; //!< filename of the input SVD file..
    svd_device_t        dvc;
    svd_regPropG_t      regProp; //!< Main properties group from the \b device element
    vector<periph_t>    v_periph;
    member_bucket_t     m_bucket;
};

#endif // SVD_STUFF_HPP_INCLUDED
